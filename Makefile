lint: isort-check black-check

isort-check:
	poetry run isort --sl -c meet_up/
	poetry run isort --sl -c tests/

black-check:
	poetry run black --check meet_up/
	poetry run black --check tests/

format:
	poetry run isort --sl meet_up/
	poetry run isort --sl tests/
	poetry run black meet_up/
	poetry run black tests/

run-tests:
	poetry run pytest tests --cov meet_up/ --cov-report=
	poetry run coverage report --omit meet_up/main.py
