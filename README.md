# Service for meeting organization MeetUp


[![pipeline status](https://gitlab.com/Mr.Geekman/mipt_backend_meetup/badges/master/pipeline.svg)](https://gitlab.com/Mr.Geekman/mipt_backend_meetup/-/commits/master) 
[![coverage report](https://gitlab.com/Mr.Geekman/mipt_backend_meetup/badges/master/coverage.svg)](https://gitlab.com/Mr.Geekman/mipt_backend_meetup/-/commits/master)

## Requirements

You can see task description at `Task Description` file.

## Installation

1. Install [poetry](https://python-poetry.org/)
2. Clone the project
3. Run installation: `poetry install`

## Development

If you want to run the tests you also have to install `postgresql` locally because testing package uses local instance of database.

## Usage

1. Prepare the configs to your needs in `configs/config.yaml`
2. Run the database, e.g. using docker: `docker compose up`
3. Create all the tables by running `poetry run main create-database`
4. Start the server using `poetry run main start-server`

## Architecture

Layers:
1. domain: `meet_up/domain`
2. data handling: `meet_up/dao`
3. service: `meet_up/services`
4. api: `meet_up/api`

Let's look at them closer.

### Domain

Here we have general abstractions that we use in our project:
* `User`
* `Meeting`
* `BusyStatus`
* `AcceptStatus`

Here we also have some basic validation of the data.

### Data handling

Here we use DAO objects to manipulate the data in database. 
I decided to use pure SQL without ORM here to see how it works and to have more control during testing.

It is important to note that on this level we don't handle the transactions, 
they are handled on service level for more universal usage of DAO methods.

### Service

This layer has business logic.

### API

This layer is responsible for 
1. Documenting the API using Swagger
2. Calling proper methods of Service layer
3. Making some validation of API input
4. Handling authorization

To check user access to the resources I use JWT-tokens ([library](https://flask-jwt-extended.readthedocs.io/en/stable/)).
If you want to access some resource that is not accessible to everyone you should
1. Get access token using `login` endpoint.
2. Save given token.
3. Pass it during request in headers like: `Authorization: Bearer <access_token>`.

## Problems

Let's discuss some problems of current solution:
1. Not all the methods was completed until the deadline.
2. It doesn't seem really RESTful:
   1. There are no links in responses;
   2. JSON is used as format for answering.
3. Too simple handling of periodicity of the meetings: 
   1. It is limited,
   2. I realized that it is probably not what was expected.
4. Too much code for a simple CRUD.
5. Probably, statuses should be placed into `Meeting` object at domain level.
6. Very limited logging.
