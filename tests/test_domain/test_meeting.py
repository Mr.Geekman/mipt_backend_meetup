import datetime
from dataclasses import asdict

import pytest

from meet_up.domain.busy_status import BusyStatus
from meet_up.domain.exceptions import ValidationError
from meet_up.domain.meeting import Meeting


@pytest.mark.parametrize(
    "init_parameters",
    [
        {
            "meeting_id": 1,
            "place": "place",
            "description": "description",
            "start_datetime": datetime.datetime.fromisoformat("2020-01-01T10:00"),
            "end_datetime": datetime.datetime.fromisoformat("2020-01-01T11:00"),
            "owner_id": 1,
            "period_week": 1,
            "participants_ids": {1, 2, 3},
            "busy_status": BusyStatus.FREE,
        },
        {
            "meeting_id": 1,
            "place": "place",
            "description": "description",
            "start_datetime": datetime.datetime.fromisoformat("2020-01-01T10:00"),
            "end_datetime": datetime.datetime.fromisoformat("2020-01-01T11:00"),
            "owner_id": 1,
            "period_week": 1,
            "participants_ids": {1, 2, 3},
        },
        {
            "place": "place",
            "description": "description",
            "start_datetime": datetime.datetime.fromisoformat("2020-01-01T10:00"),
            "end_datetime": datetime.datetime.fromisoformat("2020-01-01T11:00"),
            "owner_id": 1,
            "period_week": 1,
            "participants_ids": {1, 2, 3},
        },
        {
            "place": "place",
            "description": "description",
            "start_datetime": datetime.datetime.fromisoformat("2020-01-01T10:00"),
            "end_datetime": datetime.datetime.fromisoformat("2020-01-01T11:00"),
            "owner_id": 1,
            "participants_ids": {1, 2, 3},
        },
        {
            "place": "place",
            "description": "description",
            "start_datetime": datetime.datetime.fromisoformat("2020-01-01T10:00"),
            "end_datetime": datetime.datetime.fromisoformat("2020-01-01T11:00"),
            "owner_id": 1,
        },
    ],
)
def test_create_ok(init_parameters):
    """Test that meeting can be created."""
    Meeting(**init_parameters)


def test_create_ok_enforce_owner():
    """Test that during creation `owner_id` is automatically added to `participants_ids`."""
    meeting = Meeting(
        place="place",
        description="description",
        start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
        end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
        owner_id=1,
        participants_ids={2, 3},
    )
    assert meeting.participants_ids == {1, 2, 3}


def test_create_fail_validation_incorrect_start_end():
    """Test that creation fails if `start_time` and `end_time` have wrong ordering."""
    with pytest.raises(ValidationError):
        _ = Meeting(
            place="place",
            description="description",
            start_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
            end_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
            owner_id=1,
        )


def test_create_fail_validation_non_positive_period_week():
    """Test that creation fails if `period_time` <= 0."""
    with pytest.raises(ValidationError):
        _ = Meeting(
            place="place",
            description="description",
            start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
            end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
            owner_id=1,
            period_week=0,
        )


@pytest.mark.parametrize(
    "update_parameters",
    [
        {"place": "new_place"},
        {"description": "new_description"},
        {"start_datetime": datetime.datetime.fromisoformat("2020-01-01T10:30")},
        {"end_datetime": datetime.datetime.fromisoformat("2020-01-01T11:30")},
        {"participants_ids": {1, 4, 5}},
        {"busy_status": BusyStatus.FREE},
        {
            "place": "new_place",
            "description": "new_description",
            "busy_status": BusyStatus.FREE,
        },
    ],
)
def test_update_ok_easy_params(update_parameters):
    """Test that update works fine for changeable parameters."""
    meeting = Meeting(
        place="place",
        description="description",
        start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
        end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
        owner_id=1,
        participants_ids={1, 2, 3},
        busy_status=BusyStatus.BUSY,
    )

    updated_meeting = meeting.update(**update_parameters)

    user_fields = asdict(meeting)
    updated_user_fields = asdict(updated_meeting)
    user_fields.update(update_parameters)
    assert updated_user_fields == user_fields


@pytest.mark.parametrize(
    "update_parameters",
    [
        {"meeting_id": 5},
        {"owner_id": 5},
        {"meeting_id": 5, "owner_id": 5},
    ],
)
def test_update_ok_not_change(update_parameters):
    """Test that parameters `meeting_id`, `owner_id` can't be updated."""
    meeting = Meeting(
        place="place",
        description="description",
        start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
        end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
        owner_id=1,
        participants_ids={1, 2, 3},
    )

    updated_meeting = meeting.update(**update_parameters)
    assert updated_meeting == meeting
