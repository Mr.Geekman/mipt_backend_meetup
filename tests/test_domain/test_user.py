from dataclasses import asdict

import pytest

from meet_up.domain.exceptions import ValidationError
from meet_up.domain.user import User


@pytest.mark.parametrize(
    "init_parameters",
    [
        {
            "user_id": 1,
            "email": "example@mail.ru",
            "name": "example_name",
            "surname": "example_surname",
            "patronymic": "example_patronymic",
            "is_admin": True,
            "password": "1234",
        },
        {
            "email": "example@mail.ru",
            "name": "example_name",
            "surname": "example_surname",
            "patronymic": "example_patronymic",
            "is_admin": True,
            "password": "1234",
        },
        {
            "email": "example@mail.ru",
            "name": "example_name",
            "surname": "example_surname",
            "patronymic": "example_patronymic",
            "password": "1234",
        },
        {
            "email": "example@mail.ru",
            "name": "example_name",
            "surname": "example_surname",
            "password": "1234",
        },
        {
            "email": "example@mail.ru",
            "name": "example_name",
            "surname": "example_surname",
            "patronymic": "example_patronymic",
            "password_hash": "1234",
        },
    ],
)
def test_create_ok(init_parameters):
    """Test that user can be created."""
    User(**init_parameters)


def test_create_password_wins():
    """Test that during creation `password` has a priority over `password_hash`."""
    password = "1234"
    password_hash = User.make_password_hash("12345")
    user = User(
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password=password,
        password_hash=password_hash,
    )
    assert user.password_hash == User.make_password_hash(password)


def test_create_fail_validation_no_password_or_hash():
    """Test that creation fails if none of `password` or `password_hash` is given."""
    with pytest.raises(ValidationError):
        _ = User(
            email="example@mail.ru", name="example_name", surname="example_surname"
        )


def test_check_password_match():
    """Test that password comparison works correct if password is correct."""
    password = "1234"
    user = User(
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password=password,
    )
    assert user.check_password(password)


def test_check_password_not_match():
    """Test that password comparison works correct if password isn't correct."""
    password = "1234"
    user = User(
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password=password,
    )
    assert not user.check_password("random_password")


@pytest.mark.parametrize(
    "update_parameters",
    [
        {"email": "new_example@mail.ru"},
        {"name": "new_example_name"},
        {"surname": "new_example_surname"},
        {"patronymic": "new_example_patronymic"},
        {
            "email": "new_example@mail.ru",
            "name": "new_example_name",
            "surname": "new_example_surname",
            "patronymic": "new_example_patronymic",
        },
    ],
)
def test_update_ok_easy_params(update_parameters):
    """Test that update works fine for email/name/surname/patronymic."""
    user = User(
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password="1234",
    )

    updated_user = user.update(**update_parameters)

    user_fields = asdict(user)
    updated_user_fields = asdict(updated_user)
    user_fields.update(update_parameters)
    assert updated_user_fields == user_fields


@pytest.mark.parametrize(
    "update_parameters, new_password",
    [
        ({"password": "12345"}, "12345"),
        ({"password_hash": User.make_password_hash("12345")}, "12345"),
        (
            {"password": "xxx", "password_hash": User.make_password_hash("12345")},
            "xxx",
        ),
    ],
)
def test_update_ok_password(update_parameters, new_password):
    """Test that `password_hash` can be replaced by both `password` and `password_hash`."""
    user = User(
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password="1234",
    )

    updated_user = user.update(**update_parameters)
    assert updated_user.check_password(new_password)


@pytest.mark.parametrize(
    "update_parameters",
    [
        {"user_id": 5},
        {"is_admin": True},
        {"user_id": 5, "is_admin": True},
    ],
)
def test_update_ok_not_change(update_parameters):
    """Test that parameters `user_id`, `is_admin` can't be updated."""
    user = User(
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password="1234",
    )

    updated_user = user.update(**update_parameters)
    assert updated_user == user
