import datetime
from unittest.mock import MagicMock

import pytest

from meet_up.config import CONFIG
from meet_up.dao.meeting import MeetingDAO
from meet_up.dao.user import UserDAO
from meet_up.domain.accept_status import AcceptStatus
from meet_up.domain.busy_status import BusyStatus
from meet_up.domain.meeting import Meeting
from meet_up.domain.user import User
from meet_up.services.exceptions import AccessError
from meet_up.services.exceptions import PreparationError
from meet_up.services.meeting import MeetingService


@pytest.mark.parametrize("owner_id", [1, 2, 100])
def test_check_access_ok_admin(one_meeting_db_connection, owner_id):
    """Test that super-user has access to any meeting."""
    user_dao = UserDAO(one_meeting_db_connection)
    meeting_dao = MeetingDAO(one_meeting_db_connection)
    super_user_email = CONFIG["super_user"]["email"]
    super_user = user_dao.get_by_email(email=super_user_email)

    user_service = MeetingService(
        meeting_dao=meeting_dao, user_dao=user_dao, logged_user_email=super_user.email
    )
    user_service._check_access(user_with_access_id=owner_id)


def test_check_access_ok_right_user(one_meeting_db_connection):
    """Test that regular user has access to his meeting."""
    user_dao = UserDAO(one_meeting_db_connection)
    meeting_dao = MeetingDAO(one_meeting_db_connection)
    owner_email = "owner@mail.ru"
    owner = user_dao.get_by_email(email=owner_email)

    user_service = MeetingService(
        meeting_dao=meeting_dao, user_dao=user_dao, logged_user_email=owner.email
    )
    user_service._check_access(user_with_access_id=owner.user_id)


def test_check_access_fail_wrong_user(one_meeting_db_connection):
    """Test that regular user doesn't have access to not his meeting."""
    user_dao = UserDAO(one_meeting_db_connection)
    meeting_dao = MeetingDAO(one_meeting_db_connection)
    owner_email = "owner@mail.ru"
    owner = user_dao.get_by_email(email=owner_email)
    other_email = "participant_2@mail.ru"
    other_user = user_dao.get_by_email(email=other_email)

    user_service = MeetingService(
        meeting_dao=meeting_dao, user_dao=user_dao, logged_user_email=other_user.email
    )

    with pytest.raises(AccessError):
        user_service._check_access(user_with_access_id=owner.user_id)


def test_get_by_id():
    """Test that service uses `MeetingDAO.get_by_id` method."""
    owner_id = 1
    participant_id = 2
    example_meeting = Meeting(
        place="place",
        description="description",
        start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
        end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
        owner_id=owner_id,
        participants_ids={owner_id, participant_id},
        period_week=1,
        busy_status=BusyStatus.NOT_HERE,
    )
    user_dao = MagicMock()
    meeting_dao = MagicMock()
    meeting_dao.get_by_id = MagicMock(return_value=example_meeting)
    meeting_service = MeetingService(meeting_dao=meeting_dao, user_dao=user_dao)

    result = meeting_service.get_by_id(meeting_id=example_meeting.meeting_id)
    assert result == example_meeting
    meeting_dao.get_by_id.assert_called_once()


# TODO: simplify this test -- it is too big
def test_create_ok():
    """Test that service uses `MeetingDAO.create.create` method."""
    owner_id = 1
    owner_email = "owner@mail.ru"
    participant_id = 2
    participant_email = "participant@mail.ru"

    example_input = {
        "place": "place",
        "description": "description",
        "start_datetime": datetime.datetime.fromisoformat("2020-01-01T10:00"),
        "end_datetime": datetime.datetime.fromisoformat("2020-01-01T11:00"),
        "owner_id": owner_id,
        "participants_emails": [owner_email, participant_email],
        "period_week": 1,
        "busy_status": BusyStatus.NOT_HERE.name,
    }
    example_input_meeting = Meeting(
        place=example_input["place"],
        description=example_input["description"],
        start_datetime=example_input["start_datetime"],
        end_datetime=example_input["end_datetime"],
        owner_id=example_input["owner_id"],
        participants_ids={owner_id, participant_id},
        period_week=1,
        busy_status=BusyStatus.NOT_HERE,
    )
    example_meeting = example_input_meeting.update(meeting_id=1)

    meeting_dao = MagicMock()
    user_dao = MagicMock()
    # set multiple values to return
    user_dao.get_by_email.side_effect = [
        User(
            user_id=owner_id,
            email=owner_email,
            name="owner",
            surname="owner",
            password="1234",
        ),
        User(
            user_id=participant_id,
            email=participant_email,
            name="participant",
            surname="participant",
            password="1234",
        ),
    ]
    meeting_dao.create.return_value = example_meeting
    meeting_service = MeetingService(meeting_dao=meeting_dao, user_dao=user_dao)

    result = meeting_service.create(**example_input)

    # check calling getting by email
    assert user_dao.get_by_email.call_count == 2

    # check calling service
    assert result == example_meeting
    meeting_dao.create.assert_called_once_with(example_input_meeting)


def test_create_fail_preparation():
    """Test that service create method fails if it has problem with data preparation."""
    owner_id = 1
    owner_email = "owner@mail.ru"
    participant_id = 2
    participant_email = "participant@mail.ru"

    example_input = {
        "place": "place",
        "description": "description",
        "start_datetime": datetime.datetime.fromisoformat("2020-01-01T10:00"),
        "end_datetime": datetime.datetime.fromisoformat("2020-01-01T11:00"),
        "owner_id": owner_id,
        "participants_emails": [owner_email, participant_email],
        "period_week": 1,
        "busy_status": "NOT EXISTING",
    }

    meeting_dao = MagicMock()
    user_dao = MagicMock()
    meeting_service = MeetingService(meeting_dao=meeting_dao, user_dao=user_dao)

    with pytest.raises(PreparationError):
        _ = meeting_service.create(**example_input)


# TODO: simplify this test -- it is too big
def test_update_ok():
    """Test that service checks the access and uses `MeetingDAO.update` method."""
    owner_id = 1
    owner_email = "owner@mail.ru"
    participant_id = 2
    participant_email = "participant@mail.ru"

    update_parameters = {
        "place": "new_place",
        "participants_emails": [owner_email, participant_email],
    }
    meeting = Meeting(
        meeting_id=1,
        place="place",
        description="description",
        start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
        end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
        owner_id=owner_id,
        participants_ids={owner_id},
        period_week=1,
        busy_status=BusyStatus.NOT_HERE,
    )
    expected_updated_meeting = meeting.update(
        place="new_place", participants_ids={owner_id, participant_id}
    )

    meeting_dao = MagicMock()
    user_dao = MagicMock()
    # set multiple values to return
    user_dao.get_by_email.side_effect = [
        User(
            user_id=owner_id,
            email=owner_email,
            name="owner",
            surname="owner",
            password="1234",
        ),
        User(
            user_id=participant_id,
            email=participant_email,
            name="participant",
            surname="participant",
            password="1234",
        ),
    ]
    meeting_dao.get_by_id.return_value = meeting
    meeting_service = MeetingService(meeting_dao=meeting_dao, user_dao=user_dao)
    meeting_service._check_access = MagicMock()

    meeting_service.update(meeting_id=1, update_parameters=update_parameters)

    # check calling getting by email
    assert user_dao.get_by_email.call_count == 2

    # check calling service
    meeting_dao.update.assert_called_once_with(expected_updated_meeting)


def test_update_fail_preparation():
    """Test that service update method fails if it has problem with data preparation."""
    example_input = {
        "place": "new_place",
        "busy_status": "NOT EXISTING",
    }

    meeting_dao = MagicMock()
    user_dao = MagicMock()
    meeting_service = MeetingService(meeting_dao=meeting_dao, user_dao=user_dao)
    meeting_service._check_access = MagicMock()

    with pytest.raises(PreparationError):
        _ = meeting_service.update(meeting_id=1, update_parameters=example_input)


def test_delete():
    """Test that service checks the access and uses `MeetingDAO.delete` method."""
    user_dao = MagicMock()
    meeting_dao = MagicMock()
    meeting_service = MeetingService(meeting_dao=meeting_dao, user_dao=user_dao)
    meeting_service._check_access = MagicMock()

    meeting_service.delete(meeting_id=1)

    meeting_service._check_access.assert_called_once()
    meeting_dao.delete.assert_called_once()


def test_read_invitation():
    """Test that service checks the access and uses `MeetingDAO.read_invitation` method."""
    user_dao = MagicMock()
    meeting_dao = MagicMock()
    meeting_dao.read_invitation.return_value = AcceptStatus.ACCEPTED
    meeting_service = MeetingService(meeting_dao=meeting_dao, user_dao=user_dao)

    accept_status = meeting_service.read_invitation(meeting_id=1, user_id=1)

    assert accept_status == AcceptStatus.ACCEPTED
    meeting_dao.read_invitation.assert_called_once()


def test_answer_invitation_ok():
    """Test that service checks the access and uses `MeetingDAO.answer_invitation` method."""
    user_dao = MagicMock()
    meeting_dao = MagicMock()
    meeting_service = MeetingService(meeting_dao=meeting_dao, user_dao=user_dao)
    meeting_service._check_access = MagicMock()
    accept_status = AcceptStatus.ACCEPTED

    result = meeting_service.answer_invitation(
        meeting_id=1, user_id=1, accept_status=accept_status.name
    )

    assert result == accept_status
    meeting_service._check_access.assert_called_once()
    meeting_dao.answer_invitation.assert_called_once()


def test_answer_invitation_fail_preparation():
    """Test that service answer_invitation method fails if it has problem with data preparation."""
    user_dao = MagicMock()
    meeting_dao = MagicMock()
    meeting_service = MeetingService(meeting_dao=meeting_dao, user_dao=user_dao)
    meeting_service._check_access = MagicMock()

    with pytest.raises(PreparationError):
        _ = meeting_service.answer_invitation(
            meeting_id=1, user_id=1, accept_status="NOT_EXISTING"
        )
