from unittest.mock import MagicMock

import pytest

from meet_up.config import CONFIG
from meet_up.dao.user import UserDAO
from meet_up.domain.user import User
from meet_up.services.exceptions import AccessError
from meet_up.services.user import UserService


@pytest.mark.parametrize("user_id", [1, 2, 100])
def test_check_access_ok_admin(one_meeting_db_connection, user_id):
    """Test that super-user has access to any user."""
    user_dao = UserDAO(one_meeting_db_connection)
    super_user_email = CONFIG["super_user"]["email"]
    super_user = user_dao.get_by_email(email=super_user_email)

    user_service = UserService(user_dao=user_dao, logged_user_email=super_user.email)
    user_service._check_access(user_with_access_id=user_id)


def test_check_access_ok_right_user(one_meeting_db_connection):
    """Test that regular user has access to his data."""
    user_dao = UserDAO(one_meeting_db_connection)
    user_email = "participant_1@mail.ru"
    user = user_dao.get_by_email(email=user_email)

    user_service = UserService(user_dao=user_dao, logged_user_email=user.email)
    user_service._check_access(user_with_access_id=user.user_id)


def test_check_access_fail_wrong_user(one_meeting_db_connection):
    """Test that regular user doesn't have access to not his data."""
    user_dao = UserDAO(one_meeting_db_connection)
    user_email = "participant_1@mail.ru"
    user = user_dao.get_by_email(email=user_email)

    other_email = "participant_2@mail.ru"
    other_user = user_dao.get_by_email(email=other_email)

    user_service = UserService(user_dao=user_dao, logged_user_email=user.email)

    with pytest.raises(AccessError):
        user_service._check_access(user_with_access_id=other_user.user_id)


def test_get_by_id():
    """Test that service uses `UserDAO.get_by_id` method."""
    example_user = User(
        user_id=1,
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password="1234",
    )
    user_dao = MagicMock()
    user_dao.get_by_id.return_value = example_user
    user_service = UserService(user_dao=user_dao)

    result = user_service.get_by_id(user_id=example_user.user_id)

    assert result == example_user
    user_dao.get_by_id.assert_called_once_with(user_id=example_user.user_id)


def test_get_by_email():
    """Test that service uses `UserDAO.get_by_email` method."""
    example_user = User(
        user_id=1,
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password="1234",
    )
    user_dao = MagicMock()
    user_dao.get_by_email.return_value = example_user
    user_service = UserService(user_dao=user_dao)

    result = user_service.get_by_email(email=example_user.email)

    assert result == example_user
    user_dao.get_by_email.assert_called_once_with(email=example_user.email)


def test_create():
    """Test that service uses `UserDAO.create.create` method."""
    example_input = {
        "email": "example@mail.ru",
        "name": "example_name",
        "surname": "example_surname",
        "patronymic": None,
        "password": "1234",
    }
    example_input_user = User(
        email=example_input["email"],
        name=example_input["name"],
        surname=example_input["surname"],
        password=example_input["password"],
    )
    example_user = example_input_user.update(user_id=1)

    user_dao = MagicMock()
    user_dao.create = MagicMock(return_value=example_user)
    user_service = UserService(user_dao=user_dao)

    result = user_service.create(**example_input)

    assert result == example_user
    user_dao.create.assert_called_once_with(example_input_user)


def test_update():
    """Test that service checks the access and uses `UserDAO.update` method."""
    user = User(
        user_id=1,
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password="example_password",
    )
    update_parameters = {"email": "new_email@mail.ru"}

    user_dao = MagicMock()
    user_dao.get_by_id.return_value = user
    user_service = UserService(user_dao=user_dao)
    user_service._check_access = MagicMock()

    user_service.update(user_id=1, update_parameters=update_parameters)

    user_service._check_access.assert_called_once()
    user_dao.get_by_id.assert_called_once_with(user_id=1)
    user_dao.update.assert_called_once_with(user.update(**update_parameters))


def test_delete():
    """Test that service checks the access and uses `UserDAO.delete` method."""
    user_dao = MagicMock()
    user_service = UserService(user_dao=user_dao)
    user_service._check_access = MagicMock()

    user_service.delete(user_id=1)

    user_service._check_access.assert_called_once()
    user_dao.delete.assert_called_once_with(user_id=1)
