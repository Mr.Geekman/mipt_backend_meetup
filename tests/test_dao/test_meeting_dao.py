import dataclasses
import datetime

import pytest

from meet_up.dao.exceptions import ConstraintsError
from meet_up.dao.exceptions import MissingIdentifier
from meet_up.dao.exceptions import NotFoundError
from meet_up.dao.meeting import MeetingDAO
from meet_up.dao.user import UserDAO
from meet_up.domain.accept_status import AcceptStatus
from meet_up.domain.busy_status import BusyStatus
from meet_up.domain.meeting import Meeting
from meet_up.domain.user import User


def test_get_by_id_ok(one_meeting_db_connection):
    """Test that `get_by_id` works fine for existing meeting."""
    dao = MeetingDAO(one_meeting_db_connection)

    with one_meeting_db_connection as connection:
        with connection.cursor() as cursor:
            cursor.execute("SELECT meeting_id from meetings;")
            meeting_id = cursor.fetchone()[0]

    with one_meeting_db_connection:
        meeting = dao.get_by_id(meeting_id=meeting_id)

    assert meeting.meeting_id == meeting_id
    assert len(meeting.participants_ids) == 4


def test_get_by_id_fail_not_found(default_db_connection):
    """Test that `get_by_id` works fine for non-existing meeting."""
    dao = MeetingDAO(default_db_connection)

    with default_db_connection:
        with pytest.raises(NotFoundError):
            _ = dao.get_by_id(meeting_id=1)


@pytest.fixture()
def example_owner() -> User:
    return User(
        email="owner@mail.ru",
        name="owner_name",
        surname="owner_surname",
        password="1234",
    )


@pytest.fixture()
def example_participant() -> User:
    return User(
        email="participant_1@mail.ru",
        name="participant_1_name",
        surname="participant_1_surname",
        password="1234",
    )


def test_create_ok(default_db_connection, example_owner, example_participant):
    """Test that `create` works fine in a normal scenario."""
    owner = UserDAO(default_db_connection).create(example_owner)
    participant = UserDAO(default_db_connection).create(example_participant)

    dao = MeetingDAO(default_db_connection)
    meeting = Meeting(
        place="place",
        description="description",
        start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
        end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
        owner_id=owner.user_id,
        participants_ids={owner.user_id, participant.user_id},
        period_week=1,
        busy_status=BusyStatus.NOT_HERE,
    )

    with default_db_connection:
        created_meeting = dao.create(meeting)

    assert created_meeting.meeting_id is not None
    created_meeting.meeting_id = None
    assert created_meeting == meeting


def _test_fail_with_exception(connection, meeting_to_create, expected_exception):
    """Test that `create` fails with certain exception."""
    dao = MeetingDAO(connection)

    with connection:
        with pytest.raises(expected_exception):
            _ = dao.create(meeting_to_create)

    # check that nothing has been added to connection table
    with connection as conn:
        with conn.cursor() as cursor:
            cursor.execute("SELECT COUNT(*) FROM meetings_users;")
            num_meetings = cursor.fetchone()[0]
            assert num_meetings == 0


def test_create_fail_length_constraints(
    default_db_connection, example_owner, example_participant
):
    """Test that `create` fails if fails creation within database length constraints."""
    owner = UserDAO(default_db_connection).create(example_owner)
    participant = UserDAO(default_db_connection).create(example_participant)

    meeting = Meeting(
        place="place" * 1000,
        description="description",
        start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
        end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
        owner_id=owner.user_id,
        participants_ids={owner.user_id, participant.user_id},
        period_week=1,
        busy_status=BusyStatus.NOT_HERE,
    )
    _test_fail_with_exception(
        connection=default_db_connection,
        meeting_to_create=meeting,
        expected_exception=ConstraintsError,
    )


def test_create_fail_owner_not_found(default_db_connection, example_participant):
    """Test that `create` fails if owner not found."""
    participant = UserDAO(default_db_connection).create(example_participant)

    # here we take non-existent owner_id
    non_existent_id = 100
    meeting = Meeting(
        place="place",
        description="description",
        start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
        end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
        owner_id=non_existent_id,
        participants_ids={non_existent_id, participant.user_id},
        period_week=1,
        busy_status=BusyStatus.NOT_HERE,
    )

    _test_fail_with_exception(
        connection=default_db_connection,
        meeting_to_create=meeting,
        expected_exception=ConstraintsError,
    )


def test_create_fail_participant_not_found(default_db_connection, example_owner):
    """Test that `create` fails if participant not found."""
    owner = UserDAO(default_db_connection).create(example_owner)

    # here we take non-existent participant
    non_existent_id = 100
    meeting = Meeting(
        place="place",
        description="description",
        start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
        end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
        owner_id=non_existent_id,
        participants_ids={owner.user_id, non_existent_id},
        period_week=1,
        busy_status=BusyStatus.NOT_HERE,
    )

    _test_fail_with_exception(
        connection=default_db_connection,
        meeting_to_create=meeting,
        expected_exception=ConstraintsError,
    )


@pytest.fixture()
def one_meeting_db_meeting_id(one_meeting_db_connection):
    """Get `meeting_id` from `one_meeting_db_connection`"""
    with one_meeting_db_connection as connection:
        with connection.cursor() as cursor:
            cursor.execute("SELECT meeting_id from meetings;")
            meeting_id = cursor.fetchone()[0]
    return meeting_id


@pytest.fixture()
def one_meeting_db_users_ids(one_meeting_db_connection):
    """Get all `user_id` values from `one_meeting_db_connection`"""
    with one_meeting_db_connection as connection:
        with connection.cursor() as cursor:
            cursor.execute("SELECT user_id from users;")
            users_ids = [x[0] for x in cursor.fetchall()]
    return users_ids


def test_update_non_users_ok(one_meeting_db_connection, one_meeting_db_meeting_id):
    """Test that `update` works fine for non-user fields in a normal scenario."""
    meeting_id = one_meeting_db_meeting_id
    dao = MeetingDAO(one_meeting_db_connection)

    update_parameters = {
        "place": "new_place",
        "description": "new_description",
        "start_datetime": datetime.datetime.fromisoformat("2021-01-01T10:00"),
        "end_datetime": datetime.datetime.fromisoformat("2021-01-01T11:00"),
        "period_week": 1,
        "busy_status": BusyStatus.BUSY,
    }

    with one_meeting_db_connection:
        meeting = dao.get_by_id(meeting_id=meeting_id)
        update_meeting_input = dataclasses.replace(meeting, **update_parameters)
        updated_meeting = dao.update(update_meeting_input)

    assert updated_meeting == update_meeting_input


def test_update_participants_ok(
    one_meeting_db_connection, one_meeting_db_meeting_id, one_meeting_db_users_ids
):
    """Test that `update` works fine for `participant_ids` in a normal scenario."""
    meeting_id = one_meeting_db_meeting_id
    users_ids = one_meeting_db_users_ids

    dao = MeetingDAO(one_meeting_db_connection)

    with one_meeting_db_connection:
        update_parameters = {
            "participants_ids": {users_ids[0], users_ids[1], users_ids[2]},
        }
        meeting = dao.get_by_id(meeting_id=meeting_id)
        update_meeting_input = dataclasses.replace(meeting, **update_parameters)
        updated_meeting = dao.update(update_meeting_input)

    assert updated_meeting == update_meeting_input


def test_update_fail_no_meeting_id(
    one_meeting_db_connection, one_meeting_db_meeting_id
):
    """Test that `update` fails with given meeting doesn't have `meeting_id` field."""
    meeting_id = one_meeting_db_meeting_id

    dao = MeetingDAO(one_meeting_db_connection)

    with one_meeting_db_connection:
        update_parameters = {
            "meeting_id": None,
            "place": "new_place",
            "description": "new_description",
        }
        meeting = dao.get_by_id(meeting_id=meeting_id)
        update_meeting_input = dataclasses.replace(meeting, **update_parameters)

        with pytest.raises(MissingIdentifier):
            _ = dao.update(update_meeting_input)


def test_update_fail_not_found(
    default_db_connection, example_owner, example_participant
):
    """Test that `update` fails when meeting doesn't exist."""
    owner = UserDAO(default_db_connection).create(example_owner)
    participant = UserDAO(default_db_connection).create(example_participant)

    dao = MeetingDAO(default_db_connection)
    non_existent_meeting = Meeting(
        meeting_id=100,
        place="place",
        description="description",
        start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
        end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
        owner_id=owner.user_id,
        participants_ids={owner.user_id, participant.user_id},
        period_week=1,
        busy_status=BusyStatus.NOT_HERE,
    )

    with default_db_connection:
        with pytest.raises(NotFoundError):
            _ = dao.update(non_existent_meeting)


def test_update_fail_length_constraints(
    one_meeting_db_connection, one_meeting_db_meeting_id
):
    """Test that `update` fails if fails creation within database length constraints."""
    meeting_id = one_meeting_db_meeting_id
    dao = MeetingDAO(one_meeting_db_connection)
    meeting = dao.get_by_id(meeting_id=meeting_id)

    # update place to be too long
    update_parameters = {
        "place": meeting.place * 1000,
    }
    update_meeting_input = dataclasses.replace(meeting, **update_parameters)

    with one_meeting_db_connection:
        with pytest.raises(ConstraintsError):
            _ = dao.update(update_meeting_input)


def test_update_fail_not_found_participant(
    one_meeting_db_connection, one_meeting_db_meeting_id, one_meeting_db_users_ids
):
    """Test that `update` fails if new participant not found."""
    meeting_id = one_meeting_db_meeting_id
    users_ids = one_meeting_db_users_ids
    dao = MeetingDAO(one_meeting_db_connection)
    meeting = dao.get_by_id(meeting_id=meeting_id)

    # update participant to non-existent user
    non_existent_id = 100
    update_parameters = {
        "participants_ids": {users_ids[0], non_existent_id},
    }
    update_meeting_input = dataclasses.replace(meeting, **update_parameters)

    with one_meeting_db_connection:
        with pytest.raises(ConstraintsError):
            _ = dao.update(update_meeting_input)


def _is_meetings_users_empty(connection, meeting_id):
    """Check if there is no connections with deleted meeting."""
    with connection as conn:
        with conn.cursor() as cursor:
            cursor.execute(
                "SELECT COUNT(*) FROM meetings_users WHERE meeting_id = %s",
                (meeting_id,),
            )


def test_delete_ok(one_meeting_db_connection, one_meeting_db_meeting_id):
    """Test that `delete` works fine in a normal scenario."""
    meeting_id = one_meeting_db_meeting_id
    dao = MeetingDAO(one_meeting_db_connection)

    dao.delete(meeting_id=meeting_id)

    with pytest.raises(NotFoundError):
        _ = dao.get_by_id(meeting_id=meeting_id)
        assert _is_meetings_users_empty(
            connection=one_meeting_db_connection, meeting_id=meeting_id
        )


def test_delete_fail_not_found(one_meeting_db_connection):
    """Test that `delete` fails if meeting doesn't exist."""
    dao = MeetingDAO(one_meeting_db_connection)
    non_existent_id = 100
    with pytest.raises(NotFoundError):
        dao.delete(meeting_id=non_existent_id)
        assert _is_meetings_users_empty(
            connection=one_meeting_db_connection, meeting_id=non_existent_id
        )


def test_read_invitation_ok(one_meeting_db_connection, one_meeting_db_meeting_id):
    """Test that `read_invitation` reads invitations."""
    meeting_id = one_meeting_db_meeting_id
    dao = MeetingDAO(one_meeting_db_connection)
    meeting = dao.get_by_id(meeting_id=meeting_id)
    owner_id = meeting.owner_id

    with one_meeting_db_connection:
        owner_status = dao.read_invitation(meeting_id=meeting_id, user_id=owner_id)

    assert owner_status == AcceptStatus.ACCEPTED


def test_read_invitation_fail_not_found_meeting(
    one_meeting_db_connection, one_meeting_db_meeting_id
):
    """Test that `read_invitation` fails to accept invitation if meeting doesn't exist."""
    meeting_id = one_meeting_db_meeting_id
    dao = MeetingDAO(one_meeting_db_connection)
    meeting = dao.get_by_id(meeting_id=meeting_id)
    owner_id = meeting.owner_id

    with one_meeting_db_connection:
        with pytest.raises(NotFoundError):
            dao.read_invitation(meeting_id=100, user_id=owner_id)


def test_read_invitation_fail_not_found_user(
    one_meeting_db_connection, one_meeting_db_meeting_id
):
    """Test that `read_invitation` fails to accept invitation if user doesn't exist."""
    meeting_id = one_meeting_db_meeting_id
    dao = MeetingDAO(one_meeting_db_connection)

    with one_meeting_db_connection:
        with pytest.raises(NotFoundError):
            dao.read_invitation(meeting_id=meeting_id, user_id=100)


@pytest.mark.parametrize(
    "accept_status",
    [AcceptStatus.THINKING, AcceptStatus.ACCEPTED, AcceptStatus.DECLINED],
)
def test_answer_invitation_ok(
    one_meeting_db_connection, one_meeting_db_meeting_id, accept_status
):
    """Test that `answer_invitation` can accept invitations."""
    meeting_id = one_meeting_db_meeting_id
    dao = MeetingDAO(one_meeting_db_connection)
    meeting = dao.get_by_id(meeting_id=meeting_id)
    user_id = list(meeting.participants_ids)[-1]

    with one_meeting_db_connection:
        dao.answer_invitation(
            meeting_id=meeting_id, user_id=user_id, accept_status=accept_status
        )

    with one_meeting_db_connection:
        with one_meeting_db_connection.cursor() as cursor:
            cursor.execute(
                "SELECT accept_status_id FROM meetings_users WHERE meeting_id = %s AND user_id = %s",
                (meeting_id, user_id),
            )
            result = cursor.fetchone()
            assert AcceptStatus(result[0]) == accept_status


def test_answer_invitation_fail_not_found_meeting(
    one_meeting_db_connection, one_meeting_db_meeting_id
):
    """Test that `answer_invitation` fails to accept invitation if meeting doesn't exist."""
    meeting_id = one_meeting_db_meeting_id
    dao = MeetingDAO(one_meeting_db_connection)
    meeting = dao.get_by_id(meeting_id=meeting_id)
    user_id = list(meeting.participants_ids)[-1]

    with one_meeting_db_connection:
        with pytest.raises(NotFoundError):
            dao.answer_invitation(
                meeting_id=100, user_id=user_id, accept_status=AcceptStatus.ACCEPTED
            )


def test_answer_invitation_fail_not_found_user(
    one_meeting_db_connection, one_meeting_db_meeting_id
):
    """Test that `answer_invitation` fails to accept invitation if user doesn't exist."""
    meeting_id = one_meeting_db_meeting_id
    dao = MeetingDAO(one_meeting_db_connection)

    with one_meeting_db_connection:
        with pytest.raises(NotFoundError):
            dao.answer_invitation(
                meeting_id=meeting_id, user_id=100, accept_status=AcceptStatus.ACCEPTED
            )
