from unittest.mock import patch

from meet_up.config import CONFIG
from meet_up.dao.database import create_tables
from meet_up.dao.database import get_connection


@patch("psycopg2.connect")
def test_get_connection(connect_mock):
    """Test that `get_connection`, calls psycopg2 to connect."""
    _ = get_connection()

    connect_mock.assert_called_once_with(**CONFIG["database"])


def test_created_users(empty_db_connection):
    """Test that table `users` is created and has exactly one row."""
    create_tables(empty_db_connection)

    with empty_db_connection as connection:
        with connection.cursor() as cursor:
            cursor.execute("SELECT COUNT(*) FROM users;")
            result = cursor.fetchone()[0]
            assert result == 1


def test_created_meetings(empty_db_connection):
    """Test that table `meetings` is created and has no rows."""
    create_tables(empty_db_connection)

    with empty_db_connection as connection:
        with connection.cursor() as cursor:
            cursor.execute("SELECT COUNT(*) FROM meetings;")
            result = cursor.fetchone()[0]
            assert result == 0


def test_created_accept_statuses(empty_db_connection):
    """Test that table `accept_statuses` is created and has 3 rows."""
    create_tables(empty_db_connection)

    with empty_db_connection as connection:
        with connection.cursor() as cursor:
            cursor.execute("SELECT COUNT(*) FROM accept_statuses;")
            result = cursor.fetchone()[0]
            assert result == 3


def test_created_busy_statuses(empty_db_connection):
    """Test that table `busy_statuses` is created and has 3 rows."""
    create_tables(empty_db_connection)

    with empty_db_connection as connection:
        with connection.cursor() as cursor:
            cursor.execute("SELECT COUNT(*) FROM busy_statuses;")
            result = cursor.fetchone()[0]
            assert result == 3


def test_created_meetings_users(empty_db_connection):
    """Test that table `meetings_users` is created and has no rows."""
    create_tables(empty_db_connection)

    with empty_db_connection as connection:
        with connection.cursor() as cursor:
            cursor.execute("SELECT COUNT(*) FROM meetings_users;")
            result = cursor.fetchone()[0]
            assert result == 0


def test_recreation(empty_db_connection):
    """Test that recreation is working fine."""
    create_tables(empty_db_connection)
    create_tables(empty_db_connection)
