import dataclasses

import pytest

from meet_up.config import CONFIG
from meet_up.dao.exceptions import ConstraintsError
from meet_up.dao.exceptions import MissingIdentifier
from meet_up.dao.exceptions import NotFoundError
from meet_up.dao.user import UserDAO
from meet_up.domain.user import User


def test_get_by_id_ok(default_db_connection):
    """Test that `get_by_id` works fine for existing user."""
    dao = UserDAO(default_db_connection)

    with default_db_connection:
        user = dao.get_by_id(user_id=1)

    assert user.user_id == 1


def test_get_by_id_fail_not_found(default_db_connection):
    """Test that `get_by_id` works fine for non-existing user."""
    dao = UserDAO(default_db_connection)

    with default_db_connection:
        with pytest.raises(NotFoundError):
            _ = dao.get_by_id(user_id=100)


def test_get_by_email_ok(default_db_connection):
    """Test that `get_by_email` works fine for existing user."""
    dao = UserDAO(default_db_connection)
    email = CONFIG["super_user"]["email"]

    with default_db_connection:
        user = dao.get_by_email(email=email)

    assert user.email == email


def test_get_by_email_fail_not_found(default_db_connection):
    """Test that `get_by_email` works fine for non-existing user."""
    dao = UserDAO(default_db_connection)

    with default_db_connection:
        with pytest.raises(NotFoundError):
            _ = dao.get_by_email(email="example@mail.ru")


def test_get_by_email_ok_no_patronymic(default_db_connection):
    """Test that `get_by_email` works fine for user with no patronymic."""
    email = "example@mail.ru"
    dao = UserDAO(default_db_connection)

    with default_db_connection as connection:
        with connection.cursor() as cursor:
            cursor.execute(
                """
                INSERT INTO users(user_id, email, password_hash, is_admin, name, surname) VALUES 
                (DEFAULT, %s, %s, %s, %s, %s);
            """,
                (email, "", False, "John", "Smith"),
            )

    with default_db_connection:
        user = dao.get_by_email(email=email)

    assert user.email == email


def test_create_ok(default_db_connection):
    """Test that `create` works fine in a normal scenario."""
    user = User(email="example@mail.ru", password="1234", name="John", surname="Smith")
    dao = UserDAO(default_db_connection)

    with default_db_connection:
        created_user = dao.create(user)

    assert created_user.user_id is not None
    created_user.user_id = None
    assert created_user == user


def test_create_fail_duplicate(default_db_connection):
    """Test that `create` fails when user already exists."""
    # we have duplicate email with superuser
    email = CONFIG["super_user"]["email"]
    user = User(email=email, name="John", surname="Smith", password="1234")
    dao = UserDAO(default_db_connection)

    with default_db_connection:
        with pytest.raises(ConstraintsError):
            _ = dao.create(user)


def test_create_fail_length_constraints(default_db_connection):
    """Test that `create` fails if fails creation within database length constraints."""
    user = User(
        email="example@mail.ru", name="John" * 1000, surname="Smith", password="1234"
    )
    dao = UserDAO(default_db_connection)

    with default_db_connection:
        with pytest.raises(ConstraintsError):
            _ = dao.create(user)


def test_update_ok(default_db_connection):
    """Test that `update` works fine in a normal scenario."""
    user = User(email="example@mail.ru", name="John", surname="Smith", password="1234")
    dao = UserDAO(default_db_connection)
    created_user = dao.create(user)

    new_email = "updated@mail.ru"
    new_password = "123456"
    update_parameters = {"email": new_email, "password": new_password}
    update_user_input = dataclasses.replace(created_user, **update_parameters)

    with default_db_connection:
        updated_user = dao.update(update_user_input)

    assert updated_user == update_user_input


def test_update_fail_no_user_id(default_db_connection):
    """Test that `update` fails with given user doesn't have `user_id` field."""
    user = User(email="example@mail.ru", name="John", surname="Smith", password="1234")
    dao = UserDAO(default_db_connection)
    _ = dao.create(user)

    new_email = "updated@mail.ru"
    new_password = "123456"
    update_parameters = {"email": new_email, "password": new_password}
    update_user_input = dataclasses.replace(user, **update_parameters)

    with default_db_connection:
        with pytest.raises(MissingIdentifier):
            _ = dao.update(update_user_input)


def test_update_fail_not_found(default_db_connection):
    """Test that `update` fails when user doesn't exist."""
    dao = UserDAO(default_db_connection)
    non_existent_user = User(
        user_id=100,
        email="example@mail.ru",
        name="John",
        surname="Smith",
        password="1234",
    )

    with default_db_connection:
        with pytest.raises(NotFoundError):
            _ = dao.update(non_existent_user)


def test_update_fail_duplicate(default_db_connection):
    """Test that `update` fails when user with this email already exists."""
    user = User(email="example@mail.ru", name="John", surname="Smith", password="1234")
    dao = UserDAO(default_db_connection)
    created_user = dao.create(user)

    existing_email = CONFIG["super_user"]["email"]
    user_update_input = User(
        user_id=created_user.user_id,
        email=existing_email,
        name="John",
        surname="Smith",
        password="1234",
    )

    with default_db_connection:
        with pytest.raises(ConstraintsError):
            _ = dao.update(user_update_input)


def test_update_fail_length_constraints(default_db_connection):
    """Test that `update` fails if fails creation within database length constraints."""
    user = User(email="example@mail.ru", name="John", surname="Smith", password="1234")
    dao = UserDAO(default_db_connection)
    created_user = dao.create(user)

    user_update_input = User(
        user_id=created_user.user_id,
        email=created_user.email,
        name="John" * 1000,
        surname="Smith",
        password="1234",
    )

    with default_db_connection:
        with pytest.raises(ConstraintsError):
            _ = dao.update(user_update_input)


def _is_meetings_users_empty(connection, user_id):
    """Check if there is no connections with deleted user."""
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT COUNT(*) FROM meetings_users WHERE user_id = %s", (user_id,)
        )
        num_rows = cursor.fetchone()[0]
        return num_rows == 0


def test_delete_ok(default_db_connection):
    """Test that `delete` works fine in a normal scenario."""
    dao = UserDAO(default_db_connection)
    email = CONFIG["super_user"]["email"]

    with default_db_connection:
        existent_user = dao.get_by_email(email=email)

        dao.delete(user_id=existent_user.user_id)

    with default_db_connection:
        with pytest.raises(NotFoundError):
            _ = dao.get_by_email(email=email)

    with default_db_connection:
        assert _is_meetings_users_empty(
            connection=default_db_connection, user_id=existent_user.user_id
        )


def test_delete_fail_not_found(default_db_connection):
    """Test that `delete` fails if user doesn't exist."""
    dao = UserDAO(default_db_connection)
    non_existent_id = 100

    with default_db_connection:
        with pytest.raises(NotFoundError):
            dao.delete(user_id=non_existent_id)

    with default_db_connection:
        assert _is_meetings_users_empty(
            connection=default_db_connection, user_id=non_existent_id
        )
