import datetime
import re
from dataclasses import asdict
from http import HTTPStatus
from typing import Any
from typing import Dict
from unittest.mock import patch

import pytest
from flask_restx import marshal

from meet_up.api.meeting import meeting_model
from meet_up.dao.exceptions import ConstraintsError
from meet_up.dao.exceptions import NotFoundError
from meet_up.domain.busy_status import BusyStatus
from meet_up.domain.meeting import Meeting
from meet_up.services.exceptions import PreparationError


@pytest.fixture()
def example_meeting_dict() -> Dict[str, Any]:
    return {
        "place": "place",
        "description": "description",
        "start_datetime": "2020-01-01T10:00",
        "end_datetime": "2020-01-01T11:00",
        "owner_id": 1,
        "period_week": 1,
        "participants_emails": ["owner@mail.ru", "participant@mail.ru"],
        "busy_status": "NOT_HERE",
    }


@pytest.fixture()
def example_meeting() -> Meeting:
    return Meeting(
        meeting_id=1,
        place="place",
        description="description",
        start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
        end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
        owner_id=1,
        participants_ids={1, 2},
        period_week=1,
        busy_status=BusyStatus.NOT_HERE,
    )


@pytest.mark.parametrize(
    "meeting",
    [
        Meeting(
            meeting_id=1,
            place="place",
            description="description",
            start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
            end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
            owner_id=1,
            participants_ids={1, 2},
            period_week=1,
            busy_status=BusyStatus.NOT_HERE,
        ),
        Meeting(
            meeting_id=1,
            place="place",
            description="description",
            start_datetime=datetime.datetime.fromisoformat("2020-01-01T10:00"),
            end_datetime=datetime.datetime.fromisoformat("2020-01-01T11:00"),
            owner_id=1,
            participants_ids={1, 2},
            busy_status=BusyStatus.FREE,
        ),
    ],
)
@patch("meet_up.api.resources.MeetingService.get_by_id")
@patch("meet_up.api.resources.get_connection")
def test_get_ok(conection_mock, service_mock, client, meeting):
    """Test that get uses `MeetingService.get_by_id` and correctly returns the result."""
    service_mock.return_value = meeting
    response = client.get("/api/meetings/1")

    assert response.status_code == HTTPStatus.OK
    service_mock.assert_called_once()

    response_dict = response.get_json()
    expected_dict = asdict(meeting)
    expected_dict["start_datetime"] = datetime.datetime.isoformat(
        expected_dict["start_datetime"]
    )
    expected_dict["end_datetime"] = datetime.datetime.isoformat(
        expected_dict["end_datetime"]
    )
    expected_dict["participants_ids"] = list(expected_dict["participants_ids"])
    expected_dict["busy_status"] = expected_dict["busy_status"].name

    assert response_dict == expected_dict


@patch("meet_up.api.resources.MeetingService.get_by_id")
@patch("meet_up.api.resources.get_connection")
def test_get_fail_not_found(connection_mock, service_mock, client):
    """Test that get shows 404 if user not found."""
    service_mock.side_effect = NotFoundError("User not found")
    response = client.get("/api/meetings/1")

    assert response.status_code == HTTPStatus.NOT_FOUND


@pytest.mark.parametrize(
    "meeting_dict, expected_meeting_dict",
    [
        (
            {
                "place": "place",
                "description": "description",
                "start_datetime": "2020-01-01T10:00",
                "end_datetime": "2020-01-01T11:00",
                "owner_id": 1,
                "period_week": 1,
                "participants_emails": ["owner@mail.ru", "participant@mail.ru"],
                "busy_status": "NOT_HERE",
            },
            {
                "place": "place",
                "description": "description",
                "start_datetime": datetime.datetime.fromisoformat("2020-01-01T10:00"),
                "end_datetime": datetime.datetime.fromisoformat("2020-01-01T11:00"),
                "owner_id": 1,
                "period_week": 1,
                "participants_emails": ["owner@mail.ru", "participant@mail.ru"],
                "busy_status": "NOT_HERE",
            },
        ),
        (
            {
                "place": "place",
                "description": "description",
                "start_datetime": "2020-01-01T10:00",
                "end_datetime": "2020-01-01T11:00",
                "owner_id": 1,
                "participants_emails": ["owner@mail.ru", "participant@mail.ru"],
                "busy_status": "NOT_HERE",
            },
            {
                "place": "place",
                "description": "description",
                "start_datetime": datetime.datetime.fromisoformat("2020-01-01T10:00"),
                "end_datetime": datetime.datetime.fromisoformat("2020-01-01T11:00"),
                "owner_id": 1,
                "period_week": None,
                "participants_emails": ["owner@mail.ru", "participant@mail.ru"],
                "busy_status": "NOT_HERE",
            },
        ),
        (
            {
                "place": "place",
                "description": "description",
                "start_datetime": "2020-01-01T10:00",
                "end_datetime": "2020-01-01T11:00",
                "owner_id": 1,
                "participants_emails": ["owner@mail.ru", "participant@mail.ru"],
                "busy_status": "NOT_HERE",
                "extra": "value",
            },
            {
                "place": "place",
                "description": "description",
                "start_datetime": datetime.datetime.fromisoformat("2020-01-01T10:00"),
                "end_datetime": datetime.datetime.fromisoformat("2020-01-01T11:00"),
                "owner_id": 1,
                "period_week": None,
                "participants_emails": ["owner@mail.ru", "participant@mail.ru"],
                "busy_status": "NOT_HERE",
            },
        ),
    ],
)
@patch("meet_up.api.resources.MeetingService.create")
@patch("meet_up.api.resources.get_connection")
def test_post_ok_service(
    connection_mock,
    service_mock,
    client,
    meeting_dict,
    expected_meeting_dict,
    example_meeting,
):
    """Test that post uses `MeetingService.create` as expected."""
    # we do it here not to fail the marshalling
    service_mock.return_value = example_meeting
    response = client.post("/api/meetings", json=meeting_dict)

    assert response.status_code == HTTPStatus.CREATED

    service_mock.assert_called_once_with(**expected_meeting_dict)


@patch("meet_up.api.resources.MeetingService.create")
@patch("meet_up.api.resources.get_connection")
def test_post_ok_marshalling(
    connection_mock, service_mock, client, example_meeting_dict, example_meeting
):
    """Test that post correctly returns the result."""
    service_mock.return_value = example_meeting
    response = client.post("/api/meetings", json=example_meeting_dict)

    assert response.status_code == HTTPStatus.CREATED

    response_dict = response.get_json()
    expected_dict = marshal(example_meeting, meeting_model)
    assert response_dict == expected_dict


@pytest.mark.parametrize(
    "meeting_dict",
    [
        {
            "place": "place",
            "description": "description",
            "start_datetime": "2020-01-01T10:00",
            "end_datetime": "2020-01-01T11:00",
            "owner_id": 1,
            "period_week": 0,
            "participants_emails": ["owner@mail.ru", "participant@mail.ru"],
            "busy_status": "NOT_HERE",
        },
        {
            "place": "place",
            "description": "description",
            "start_datetime": "not a valid time",
            "end_datetime": "2020-01-01T11:00",
            "owner_id": 1,
            "participants_emails": ["owner@mail.ru", "participant@mail.ru"],
            "busy_status": "NOT_HERE",
        },
    ],
)
@patch("meet_up.api.resources.get_connection")
def test_post_fail_validation(connection_mock, client, meeting_dict):
    """Test that post fails in case of validation errors and shows bad request."""
    response = client.post("/api/meetings", json=meeting_dict)

    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Input payload validation failed"
    message = result["message"]
    assert expected_message == message


@patch("meet_up.api.resources.MeetingService.create")
@patch("meet_up.api.resources.get_connection")
def test_post_fail_constraints(
    connection_mock, service_mock, client, example_meeting_dict
):
    """Test that post fails in case of breaking database constraints and shows bad request."""
    service_mock.side_effect = ConstraintsError("Something is broken")
    response = client.post("/api/meetings", json=example_meeting_dict)

    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Something is broken"
    message = result["message"]
    assert expected_message == message


@patch("meet_up.api.resources.MeetingService.create")
@patch("meet_up.api.resources.get_connection")
def test_post_fail_preparation(
    connection_mock, service_mock, client, example_meeting_dict
):
    """Test that post fails in case of preparation fail and shows bad request."""
    service_mock.side_effect = PreparationError("Something is broken")
    response = client.post("/api/meetings", json=example_meeting_dict)

    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Something is broken"
    message = result["message"]
    assert expected_message == message


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.update")
@patch("meet_up.api.resources.get_connection")
def test_patch_ok_marshalling(
    connection_mock,
    service_mock,
    verification_mock,
    get_identity_mock,
    client,
    example_meeting,
):
    """Test that patch correctly returns the result."""
    service_mock.return_value = example_meeting
    response = client.patch("/api/meetings/1", json={})

    assert response.status_code == HTTPStatus.OK

    response_dict = response.get_json()
    expected_dict = marshal(example_meeting, meeting_model)
    assert response_dict == expected_dict


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.update")
@patch("meet_up.api.resources.get_connection")
def test_patch_ok_authentication(
    connection_mock,
    service_mock,
    verification_mock,
    get_identity_mock,
    client,
    example_meeting,
):
    """Test that patch checks that user is logged in."""
    service_mock.return_value = example_meeting
    response = client.patch("/api/meetings/1", json={})

    assert response.status_code == HTTPStatus.OK

    verification_mock.assert_called_once()
    get_identity_mock.assert_called_once()


@pytest.mark.parametrize(
    "update_meeting_dict",
    [
        {
            "period_week": 0,
        },
        {
            "start_datetime": "not a valid time",
        },
    ],
)
@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.get_connection")
def test_patch_fail_validation(
    connection_mock, verification_mock, get_identity_mock, client, update_meeting_dict
):
    """Test that patch fails in case of validation errors and shows bad request."""
    response = client.patch("/api/meetings/1", json=update_meeting_dict)
    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Input payload validation failed"
    message = result["message"]
    assert expected_message == message


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.update")
@patch("meet_up.api.resources.get_connection")
def test_patch_fail_constraints(
    connection_mock, verification_mock, get_identity_mock, service_mock, client
):
    """Test that patch fails in case of breaking database constraints and shows bad request."""
    service_mock.side_effect = ConstraintsError("Something is broken")
    response = client.patch("/api/meetings/1", json={})

    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Something is broken"
    message = result["message"]
    assert expected_message == message


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.create")
@patch("meet_up.api.resources.get_connection")
def test_post_fail_preparation(
    connection_mock, verification_mock, get_identity_mock, service_mock, client
):
    """Test that patch fails in case of preparation fail and shows bad request."""
    service_mock.side_effect = PreparationError("Something is broken")
    response = client.patch("/api/meetings/1", json={})

    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Something is broken"
    message = result["message"]
    assert expected_message == message


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.update")
@patch("meet_up.api.resources.get_connection")
def test_patch_fail_not_found(
    connection_mock, service_mock, verification_mock, get_identity_mock, client
):
    """Test that patch fails in case of not found error and shows 404."""
    service_mock.side_effect = NotFoundError("Meeting not found")
    response = client.patch("/api/meetings/1", json={})

    assert response.status_code == HTTPStatus.NOT_FOUND

    result = response.get_json()
    expected_message = "Meeting not found"
    message = result["message"]
    assert re.match(expected_message, message)


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.delete")
@patch("meet_up.api.resources.get_connection")
def test_delete_ok_service(
    connection_mock, service_mock, verification_mock, get_identity_mock, client
):
    """Test that delete uses `MeetingService.delete` as expected."""
    response = client.delete("/api/meetings/1")

    assert response.status_code == HTTPStatus.NO_CONTENT
    service_mock.assert_called_once_with(meeting_id=1)


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.delete")
@patch("meet_up.api.resources.get_connection")
def test_delete_ok_authentication(
    connection_mock, service_mock, verification_mock, get_identity_mock, client
):
    """Test that patch checks that user is logged in."""
    response = client.delete("/api/meetings/1")

    verification_mock.assert_called_once()
    get_identity_mock.assert_called_once()


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.delete")
@patch("meet_up.api.resources.get_connection")
def test_delete_fail_not_found(
    connection_mock, service_mock, verification_mock, get_identity_mock, client
):
    """Test that delete shows 404 if meeting not found"""
    service_mock.side_effect = NotFoundError("Meeting not found")
    response = client.delete("/api/meetings/1")

    assert response.status_code == HTTPStatus.NOT_FOUND

    result = response.get_json()
    expected_message = "Meeting not found"
    message = result["message"]
    assert re.match(expected_message, message)
