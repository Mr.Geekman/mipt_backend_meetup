import re
from dataclasses import asdict
from http import HTTPStatus
from typing import Any
from typing import Dict
from unittest.mock import patch

import pytest
from flask_restx import marshal

from meet_up.api.invitation import invitation_model
from meet_up.api.user import user_model
from meet_up.dao.exceptions import ConstraintsError
from meet_up.dao.exceptions import NotFoundError
from meet_up.domain.accept_status import AcceptStatus
from meet_up.domain.user import User
from meet_up.services.exceptions import PreparationError


@pytest.mark.parametrize(
    "accept_status",
    [AcceptStatus.THINKING, AcceptStatus.ACCEPTED, AcceptStatus.DECLINED],
)
@patch("meet_up.api.resources.MeetingService.read_invitation")
@patch("meet_up.api.resources.get_connection")
def test_get_ok(connection_mock, service_mock, client, accept_status):
    """Test that get uses `MeetingService.read_invitation` and correctly returns the result."""
    service_mock.return_value = accept_status
    response = client.get("/api/meetings/1/users/1/invitation")

    assert response.status_code == HTTPStatus.OK
    service_mock.assert_called_once()

    response_dict = response.get_json()
    expected_dict = marshal(accept_status, invitation_model)
    assert response_dict == expected_dict


@patch("meet_up.api.resources.MeetingService.read_invitation")
@patch("meet_up.api.resources.get_connection")
def test_get_fail_not_found(connection_mock, service_mock, client):
    """Test that get shows 404 if we have not found error."""
    service_mock.side_effect = NotFoundError("Meeting not found")

    response = client.get("/api/meetings/1/users/1/invitation")

    assert response.status_code == HTTPStatus.NOT_FOUND

    result = response.get_json()
    expected_message = "Meeting not found"
    message = result["message"]
    assert re.match(expected_message, message)


@pytest.mark.parametrize(
    "accept_status",
    [AcceptStatus.THINKING, AcceptStatus.ACCEPTED, AcceptStatus.DECLINED],
)
@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.answer_invitation")
@patch("meet_up.api.resources.get_connection")
def test_put_ok_service(
    connection_mock,
    service_mock,
    verification_mock,
    get_identity_mock,
    client,
    accept_status,
):
    """Test that put uses `MeetingService.create` as expected."""
    service_mock.return_value = accept_status.name
    invitation_dict = {"status": accept_status.name}
    response = client.put("/api/meetings/1/users/1/invitation", json=invitation_dict)

    assert response.status_code == HTTPStatus.CREATED

    service_mock.assert_called_once_with(
        meeting_id=1, user_id=1, accept_status=accept_status.name
    )


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.answer_invitation")
@patch("meet_up.api.resources.get_connection")
def test_put_ok_marshalling(
    connection_mock,
    service_mock,
    verification_mock,
    get_identity_mock,
    client,
):
    """Test that post correctly returns the result."""
    accept_status = AcceptStatus.ACCEPTED
    service_mock.return_value = accept_status
    response = client.put(
        "/api/meetings/1/users/1/invitation",
        json={"status": accept_status.name},
    )

    assert response.status_code == HTTPStatus.CREATED

    response_dict = response.get_json()
    expected_dict = marshal(accept_status, invitation_model)
    assert response_dict == expected_dict


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.answer_invitation")
@patch("meet_up.api.resources.get_connection")
def test_put_ok_authentication(
    connection_mock,
    service_mock,
    verification_mock,
    get_identity_mock,
    client,
):
    """Test that put checks that user is logged in."""
    service_mock.return_value = AcceptStatus.ACCEPTED
    response = client.put(
        "/api/meetings/1/users/1/invitation",
        json={"status": AcceptStatus.ACCEPTED.name},
    )

    assert response.status_code == HTTPStatus.CREATED

    verification_mock.assert_called_once()
    get_identity_mock.assert_called_once()


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.answer_invitation")
@patch("meet_up.api.resources.get_connection")
def test_put_fail_not_found(
    connection_mock, service_mock, verification_mock, get_identity_mock, client
):
    """Test that put fails in case of not found error and shows 404."""
    service_mock.side_effect = NotFoundError("Meeting not found")
    response = client.put(
        "/api/meetings/1/users/1/invitation",
        json={"status": AcceptStatus.ACCEPTED.name},
    )

    assert response.status_code == HTTPStatus.NOT_FOUND

    result = response.get_json()
    expected_message = "Meeting not found"
    message = result["message"]
    assert re.match(expected_message, message)


@pytest.mark.parametrize(
    "invitation_dict",
    [
        {},
    ],
)
@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.get_connection")
def test_patch_fail_validation(
    connection_mock, verification_mock, get_identity_mock, client, invitation_dict
):
    """Test that put fails in case of validation errors and shows bad request."""
    response = client.put(
        "/api/meetings/1/users/1/invitation",
        json=invitation_dict,
    )
    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Input payload validation failed"
    message = result["message"]
    assert expected_message == message


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.MeetingService.answer_invitation")
@patch("meet_up.api.resources.get_connection")
def test_put_fail_preparation_error(
    connection_mock, service_mock, verification_mock, get_identity_mock, client
):
    """Test that put fails in case of preparation fail and shows bad request."""
    service_mock.side_effect = PreparationError("Something is broken")
    response = client.put(
        "/api/meetings/1/users/1/invitation",
        json={"status": AcceptStatus.ACCEPTED.name},
    )

    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Something is broken"
    message = result["message"]
    assert expected_message == message
