import pytest

from meet_up.api.app import app


@pytest.fixture()
def client():
    client = app.test_client()

    yield client
