import re
from http import HTTPStatus
from typing import Any
from typing import Dict
from unittest.mock import patch

import pytest
from flask_restx import marshal

from meet_up.api.user import user_model
from meet_up.dao.exceptions import ConstraintsError
from meet_up.dao.exceptions import NotFoundError
from meet_up.domain.user import User


@pytest.fixture()
def example_user_dict() -> Dict[str, Any]:
    return {
        "email": "example@mail.ru",
        "name": "example_name",
        "surname": "example_surname",
        "password": "1234",
    }


@pytest.fixture()
def example_user() -> User:
    return User(
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password="1234",
    )


@patch("meet_up.api.resources.UserService.get_by_email")
@patch("meet_up.api.resources.get_connection")
def test_login_ok(connection_mock, service_mock, client):
    """Test that login works fine for correct credentials."""
    password = "1234"
    user = User(
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password=password,
    )
    service_mock.return_value = user
    credentials = {"email": user.email, "password": password}

    response = client.post("/api/login", json=credentials)

    assert response.status_code == HTTPStatus.CREATED

    result = response.get_json()
    assert "access_token" in result
    service_mock.assert_called_once_with(email=user.email)


@patch("meet_up.api.resources.UserService.get_by_email")
@patch("meet_up.api.resources.get_connection")
def test_login_fail_not_found_user(connection_mock, service_mock, client):
    """Test that login fails if unknown user is given."""
    service_mock.side_effect = NotFoundError("User not found")
    credentials = {"email": "example@mail.ru", "password": "1234"}

    response = client.post("/api/login", json=credentials)

    assert response.status_code == HTTPStatus.UNAUTHORIZED

    result = response.get_json()
    expected_message = "Unknown user"
    message = result["message"]
    assert re.match(expected_message, message)


@patch("meet_up.api.resources.UserService.get_by_email")
@patch("meet_up.api.resources.get_connection")
def test_login_fail_wrong_password(connection_mock, service_mock, client):
    """Test that login fails if wrong password is given."""
    user = User(
        email="example@mail.ru",
        name="example_name",
        surname="example_surname",
        password="1234",
    )
    service_mock.return_value = user
    credentials = {"email": user.email, "password": "12345"}

    response = client.post("/api/login", json=credentials)

    assert response.status_code == HTTPStatus.UNAUTHORIZED

    result = response.get_json()
    expected_message = "Wrong password"
    message = result["message"]
    assert re.match(expected_message, message)


@pytest.mark.parametrize(
    "credentials",
    [
        {"email": "not_email", "password": "1234"},
        {"email": "example@mail.ru"},
        {"password": "1234"},
    ],
)
@patch("meet_up.api.resources.get_connection")
def test_login_fail_validation(connection_mock, client, credentials):
    """Test that login fails if fields validation failed."""
    response = client.post("/api/login", json=credentials)

    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Input payload validation failed"
    message = result["message"]
    assert message == expected_message


@patch("meet_up.api.resources.get_connection")
def test_authorization_integration(connection_mock, client, default_db_connection):
    """Test authorization in a simple scenario."""
    connection_mock.return_value = default_db_connection
    response = client.post(
        "/api/users",
        json={
            "email": "new@mail.ru",
            "name": "example_name",
            "surname": "example_surname",
            "password": "1234",
        },
    )
    assert response.status_code == HTTPStatus.CREATED
    new_user = response.get_json()

    response = client.post(
        "/api/login", json={"email": "new@mail.ru", "password": "1234"}
    )
    assert response.status_code == HTTPStatus.CREATED
    access_token = response.get_json()

    response = client.delete(
        f"/api/users/{new_user['user_id']}",
        headers={"Authorization": f"Bearer {access_token['access_token']}"},
    )
    assert response.status_code == HTTPStatus.NO_CONTENT

    response = client.get(f"/api/users/{new_user['user_id']}")
    assert response.status_code == HTTPStatus.NOT_FOUND


@pytest.mark.parametrize(
    "user",
    [
        User(
            user_id=1,
            email="example@mail.ru",
            name="example_name",
            surname="example_surname",
            password="1234",
        ),
        User(
            user_id=1,
            email="example@mail.ru",
            name="example_name",
            surname="example_surname",
            patronymic="example_patronymic",
            password="1234",
        ),
    ],
)
@patch("meet_up.api.resources.UserService.get_by_id")
@patch("meet_up.api.resources.get_connection")
def test_get_ok(connection_mock, service_mock, client, user):
    """Test that get uses `UserService.get_by_id` and correctly returns the result."""
    service_mock.return_value = user
    response = client.get("/api/users/1")

    assert response.status_code == HTTPStatus.OK
    service_mock.assert_called_once()

    response_dict = response.get_json()
    expected_dict = marshal(user, user_model)
    assert response_dict == expected_dict


@patch("meet_up.api.resources.UserService.get_by_id")
@patch("meet_up.api.resources.get_connection")
def test_get_fail_not_found(connection_mock, service_mock, client):
    """Test that get shows 404 if user not found."""
    service_mock.side_effect = NotFoundError("User not found")

    response = client.get("/api/users/1")

    assert response.status_code == HTTPStatus.NOT_FOUND

    result = response.get_json()
    expected_message = "User not found"
    message = result["message"]
    assert re.match(expected_message, message)


@pytest.mark.parametrize(
    "user_dict, expected_user_dict",
    [
        (
            {
                "email": "example@mail.ru",
                "name": "example_name",
                "surname": "example_surname",
                "password": "1234",
            },
            {
                "email": "example@mail.ru",
                "name": "example_name",
                "surname": "example_surname",
                "password": "1234",
                "patronymic": None,
            },
        ),
        (
            {
                "email": "example@mail.ru",
                "name": "example_name",
                "surname": "example_surname",
                "password": "1234",
                "patronymic": "example_patronymic",
            },
            {
                "email": "example@mail.ru",
                "name": "example_name",
                "surname": "example_surname",
                "password": "1234",
                "patronymic": "example_patronymic",
            },
        ),
        (
            {
                "email": "example@mail.ru",
                "name": "example_name",
                "surname": "example_surname",
                "password": "1234",
                "patronymic": "example_patronymic",
                "extra": "111",
            },
            {
                "email": "example@mail.ru",
                "name": "example_name",
                "surname": "example_surname",
                "password": "1234",
                "patronymic": "example_patronymic",
            },
        ),
    ],
)
@patch("meet_up.api.resources.UserService.create")
@patch("meet_up.api.resources.get_connection")
def test_post_ok_service(
    connection_mock, service_mock, client, user_dict, expected_user_dict, example_user
):
    """Test that post uses `UserService.create` as expected."""
    service_mock.return_value = example_user
    response = client.post("/api/users", json=user_dict)

    assert response.status_code == HTTPStatus.CREATED

    service_mock.assert_called_once_with(**expected_user_dict)


@patch("meet_up.api.resources.UserService.create")
@patch("meet_up.api.resources.get_connection")
def test_post_ok_marshalling(
    connection_mock, service_mock, client, example_user_dict, example_user
):
    """Test that post correctly returns the result."""
    # we do it here not to fail the marshalling
    service_mock.return_value = example_user
    response = client.post("/api/users", json=example_user_dict)

    assert response.status_code == HTTPStatus.CREATED

    response_dict = response.get_json()
    expected_dict = marshal(example_user, user_model)
    assert response_dict == expected_dict


@pytest.mark.parametrize(
    "user_dict",
    [
        {
            "email": "not_email",
            "name": "example_name",
            "surname": "example_surname",
            "password": "1234",
        },
        {
            "email": "example@mail.ru",
            "surname": "example_surname",
            "password": "1234",
        },
        {
            "surname": "example_surname",
            "password": "1234",
        },
    ],
)
@patch("meet_up.api.resources.get_connection")
def test_post_fail_validation(connection_mock, client, user_dict):
    """Test that post fails in case of validation errors and shows bad request."""
    response = client.post("/api/users", json=user_dict)

    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Input payload validation failed"
    message = result["message"]
    assert expected_message == message


@patch("meet_up.api.resources.UserService.create")
@patch("meet_up.api.resources.get_connection")
def test_post_fail_constraints(
    connection_mock, service_mock, client, example_user_dict
):
    """Test that post fails in case of breaking database constraints and shows bad request."""
    service_mock.side_effect = ConstraintsError("Something is broken")
    response = client.post("/api/users", json=example_user_dict)

    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Something is broken"
    message = result["message"]
    assert expected_message == message


# TODO: split it into 3 different tests
@pytest.mark.parametrize(
    "update_user_dict, expected_update_user_dict",
    [
        (
            {
                "email": "example_new@mail.ru",
            },
            {
                "email": "example_new@mail.ru",
            },
        ),
        (
            {
                "name": "example_name_new",
            },
            {
                "name": "example_name_new",
            },
        ),
        (
            {
                "surname": "example_surname_new",
            },
            {
                "surname": "example_surname_new",
            },
        ),
        (
            {
                "patronymic": "example_patronymic_new",
            },
            {
                "patronymic": "example_patronymic_new",
            },
        ),
        (
            {
                "patronymic": None,
            },
            {
                "patronymic": None,
            },
        ),
        (
            {},
            {},
        ),
        (
            {"extra": "value"},
            {},
        ),
    ],
)
@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.UserService.update")
@patch("meet_up.api.resources.get_connection")
def test_patch_ok_service(
    connection_mock,
    service_mock,
    verification_mock,
    get_identity_mock,
    client,
    update_user_dict,
    expected_update_user_dict,
    example_user,
):
    """Test that patch uses `UserService.update` as expected."""
    # we do it here not to fail the marshalling
    service_mock.return_value = example_user
    response = client.patch("/api/users/1", json=update_user_dict)

    assert response.status_code == HTTPStatus.OK

    # check how UserService.create is called
    service_mock.assert_called_once_with(
        user_id=1, update_parameters=expected_update_user_dict
    )


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.UserService.update")
@patch("meet_up.api.resources.get_connection")
def test_patch_ok_marshalling(
    connection_mock,
    service_mock,
    verification_mock,
    get_identity_mock,
    client,
    example_user,
):
    """Test that patch correctly returns the result."""
    service_mock.return_value = example_user
    response = client.patch("/api/users/1", json={})

    assert response.status_code == HTTPStatus.OK

    response_dict = response.get_json()
    expected_dict = marshal(example_user, user_model)
    assert response_dict == expected_dict


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.UserService.update")
@patch("meet_up.api.resources.get_connection")
def test_patch_ok_authentication(
    connection_mock,
    service_mock,
    verification_mock,
    get_identity_mock,
    client,
    example_user,
):
    """Test that patch checks that user is logged in."""
    service_mock.return_value = example_user
    response = client.patch("/api/users/1", json={})

    assert response.status_code == HTTPStatus.OK

    verification_mock.assert_called_once()
    get_identity_mock.assert_called_once()


@pytest.mark.parametrize(
    "update_user_dict",
    [
        {
            "email": "not_email",
        },
    ],
)
@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.get_connection")
def test_patch_fail_validation(
    connection_mock, verification_mock, get_identity_mock, client, update_user_dict
):
    """Test that patch fails in case of validation errors and shows bad request."""
    response = client.patch("/api/users/1", json=update_user_dict)
    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Input payload validation failed"
    message = result["message"]
    assert expected_message == message


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.UserService.update")
@patch("meet_up.api.resources.get_connection")
def test_patch_fail_constraints(
    connection_mock, verification_mock, get_identity_mock, service_mock, client
):
    """Test that patch fails in case of breaking database constraints and shows bad request."""
    service_mock.side_effect = ConstraintsError("Something is broken")
    response = client.patch("/api/users/1", json={})

    assert response.status_code == HTTPStatus.BAD_REQUEST

    result = response.get_json()
    expected_message = "Something is broken"
    message = result["message"]
    assert expected_message == message


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.UserService.update")
@patch("meet_up.api.resources.get_connection")
def test_patch_fail_not_found(
    connection_mock, service_mock, verification_mock, get_identity_mock, client
):
    """Test that patch fails in case of not found error and shows 404."""
    service_mock.side_effect = NotFoundError("User not found")
    response = client.patch("/api/users/1", json={})

    assert response.status_code == HTTPStatus.NOT_FOUND

    result = response.get_json()
    expected_message = "User not found"
    message = result["message"]
    assert re.match(expected_message, message)


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.UserService.delete")
@patch("meet_up.api.resources.get_connection")
def test_delete_ok_service(
    connection_mock, service_mock, verification_mock, get_identity_mock, client
):
    """Test that delete uses `UserService.delete` as expected."""
    response = client.delete("/api/users/1")

    assert response.status_code == HTTPStatus.NO_CONTENT
    service_mock.assert_called_once_with(user_id=1)


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.UserService.delete")
@patch("meet_up.api.resources.get_connection")
def test_delete_ok_authentication(
    connection_mock, service_mock, verification_mock, get_identity_mock, client
):
    """Test that patch checks that user is logged in."""
    response = client.delete("/api/users/1")

    verification_mock.assert_called_once()
    get_identity_mock.assert_called_once()


@patch("meet_up.api.resources.get_jwt_identity")
@patch("flask_jwt_extended.view_decorators.verify_jwt_in_request")
@patch("meet_up.api.resources.UserService.delete")
@patch("meet_up.api.resources.get_connection")
def test_delete_fail_not_found(
    connection_mock, service_mock, verification_mock, get_identity_mock, client
):
    """Test that delete shows 404 if user not found"""
    service_mock.side_effect = NotFoundError("User not found")
    response = client.delete("/api/users/1")

    assert response.status_code == HTTPStatus.NOT_FOUND

    result = response.get_json()
    expected_message = "User not found"
    message = result["message"]
    assert re.match(expected_message, message)
