import datetime

import psycopg2
import pytest
import testing.postgresql

from meet_up.dao.database import create_tables
from meet_up.dao.user import UserDAO
from meet_up.domain.accept_status import AcceptStatus
from meet_up.domain.busy_status import BusyStatus
from meet_up.domain.user import User


# TODO: may be move all connections to conftest inside DAO
@pytest.fixture()
def empty_db_connection():
    """Create connection to empty database."""
    postgresql = testing.postgresql.Postgresql()
    connection = psycopg2.connect(**postgresql.dsn())

    yield connection

    postgresql.stop()


@pytest.fixture()
def default_db_connection(empty_db_connection):
    """Create connection to filled by default database."""
    create_tables(empty_db_connection)
    return empty_db_connection


@pytest.fixture()
def one_meeting_db_connection(default_db_connection):
    """Create connection to database with few users and one meeting."""
    owner_input = User(
        email="owner@mail.ru",
        name="owner_name",
        surname="owner_surname",
        password="1234",
    )
    owner = UserDAO(default_db_connection).create(owner_input)

    participant_1_input = User(
        email="participant_1@mail.ru",
        name="participant_1_name",
        surname="participant_1_surname",
        password="1234",
    )
    participant_1 = UserDAO(default_db_connection).create(participant_1_input)

    participant_2_input = User(
        email="participant_2@mail.ru",
        name="participant_2_name",
        surname="participant_2_surname",
        password="1234",
    )
    participant_2 = UserDAO(default_db_connection).create(participant_2_input)

    participant_3_input = User(
        email="participant_3@mail.ru",
        name="participant_3_name",
        surname="participant_3_surname",
        password="1234",
    )
    participant_3 = UserDAO(default_db_connection).create(participant_3_input)

    participant_4_input = User(
        email="participant_4@mail.ru",
        name="participant_4_name",
        surname="participant_4_surname",
        password="1234",
    )
    participant_4 = UserDAO(default_db_connection).create(participant_4_input)

    with default_db_connection.cursor() as cursor:
        # add meeting
        cursor.execute(
            """
            INSERT INTO meetings(meeting_id, place, description, start_datetime, end_datetime, period_week, owner_id, busy_status_id) VALUES 
            (DEFAULT, %s, %s, %s, %s, %s, %s, %s)
            RETURNING meeting_id;
        """,
            (
                "place",
                "description",
                datetime.datetime.fromisoformat("2020-01-01T10:00"),
                datetime.datetime.fromisoformat("2020-01-01T11:00"),
                None,
                owner.user_id,
                BusyStatus.NOT_HERE.value,
            ),
        )
        meeting_id = cursor.fetchone()[0]

        # add statuses for meeting
        cursor.execute(
            "INSERT INTO meetings_users(user_id, meeting_id, accept_status_id) VALUES (%s, %s, %s)",
            (owner.user_id, meeting_id, AcceptStatus.ACCEPTED.value),
        )
        cursor.execute(
            "INSERT INTO meetings_users(user_id, meeting_id, accept_status_id) VALUES (%s, %s, %s)",
            (participant_1.user_id, meeting_id, AcceptStatus.THINKING.value),
        )
        cursor.execute(
            "INSERT INTO meetings_users(user_id, meeting_id, accept_status_id) VALUES (%s, %s, %s)",
            (participant_2.user_id, meeting_id, AcceptStatus.ACCEPTED.value),
        )
        cursor.execute(
            "INSERT INTO meetings_users(user_id, meeting_id, accept_status_id) VALUES (%s, %s, %s)",
            (participant_3.user_id, meeting_id, AcceptStatus.DECLINED.value),
        )

    return default_db_connection
