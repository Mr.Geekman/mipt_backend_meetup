class AccessError(Exception):
    """Problem with access to the object."""

    def __init__(self, message: str):
        super().__init__(message)


class PreparationError(Exception):
    """Problem with data preparation before applying DAO."""

    def __init__(self, message: str):
        super().__init__(message)
