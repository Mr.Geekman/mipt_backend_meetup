import datetime
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import Set

from meet_up.dao.meeting import MeetingDAO
from meet_up.dao.user import UserDAO
from meet_up.domain.accept_status import AcceptStatus
from meet_up.domain.busy_status import BusyStatus
from meet_up.domain.meeting import Meeting
from meet_up.services.exceptions import AccessError
from meet_up.services.exceptions import PreparationError


class MeetingService:
    """Services related to meeting.

    This class also handles the transactions over DAO objects.
    """

    def __init__(
        self,
        meeting_dao: MeetingDAO,
        user_dao: UserDAO,
        logged_user_email: Optional[str] = None,
    ):
        """Init object.

        :param meeting_dao: DAO for meeting
        :param user_dao: DAO for user
        :param logged_user_email: email of service user
        """
        self.meeting_dao = meeting_dao
        self.user_dao = user_dao
        self.connection = meeting_dao.connection
        self.logged_user_email = logged_user_email

    def _check_access(self, user_with_access_id: int):
        """Check if service user has access to the operation.

        :param user_with_access_id: user that can do this action
        """
        logged_user = self.user_dao.get_by_email(email=self.logged_user_email)
        if self.logged_user_email is None or (
            not logged_user.is_admin and user_with_access_id != logged_user.user_id
        ):
            raise AccessError(
                f"Only user with user_id={user_with_access_id} can make this operation"
            )

    def _prepare_busy_status(self, busy_status_name: str) -> BusyStatus:
        try:
            return BusyStatus[busy_status_name]
        except KeyError:
            raise PreparationError(
                f"This busy_status doesn't exist: {busy_status_name}"
            )

    def _prepare_accept_status(self, accept_status_name: str) -> AcceptStatus:
        try:
            return AcceptStatus[accept_status_name]
        except KeyError:
            raise PreparationError(
                f"This accept_status doesn't exist: {accept_status_name}"
            )

    def _prepare_participants_ids(self, participants_emails: List[str]) -> Set[int]:
        return {
            self.user_dao.get_by_email(email=email).user_id
            for email in set(participants_emails)
        }

    def get_by_id(self, meeting_id: int) -> Meeting:
        """Get meeting by id.

        :param meeting_id: id to search

        :return: found meeting

        :raise NotFoundError: if meeting doesn't exist
        :raise ValidationError: if meeting creation fails
        """
        with self.connection:
            meeting = self.meeting_dao.get_by_id(meeting_id=meeting_id)
        return meeting

    def create(
        self,
        place: str,
        description: str,
        start_datetime: datetime.datetime,
        end_datetime: datetime.datetime,
        owner_id: int,
        busy_status: str,
        period_week: Optional[int],
        participants_emails: List[str],
    ) -> Meeting:
        """Create meeting.

        :param place: place of the meeting
        :param description: description of the meeting
        :param start_datetime: datetime of start of the meeting
        :param end_datetime: datetime of end of the meeting
        :param owner_id: id of the owner of the meeting
        :param busy_status: busyness status of the meeting
        :param period_week:
        :param participants_emails: emails of participants

        :return: created meeting

        :raise NotFoundError: if participant with this email doesn't exist
        :raise PreparationError: if preparation of busy_status to the DAO object fails
        :raise ValidationError: if meeting creation fails
        :raise ConstraintsError: if string length constraints are broken
        :raise ConstraintsError: if foreign key constraints are broken
        """
        # make parameters preparation
        busy_status_enum = self._prepare_busy_status(busy_status_name=busy_status)
        participants_ids = self._prepare_participants_ids(
            participants_emails=participants_emails
        )

        # create a meeting
        meeting = Meeting(
            place=place,
            description=description,
            start_datetime=start_datetime,
            end_datetime=end_datetime,
            owner_id=owner_id,
            busy_status=busy_status_enum,
            period_week=period_week,
            participants_ids=participants_ids,
        )

        with self.connection:
            created_meeting = self.meeting_dao.create(meeting)
        return created_meeting

    def update(self, meeting_id: int, update_parameters: Dict[str, Any]) -> Meeting:
        """Update meeting.

        :param meeting_id: meeting id to update

        :return: updated meeting

        :raise AccessError: if meeting can't be accessed by service user
        :raise NotFoundError: if participant with this email doesn't exist
        :raise PreparationError: if preparation of busy_status to the DAO object fails
        :raise ValidationError: if meeting creation fails
        :raise MissingIdentifier: if given meeting doesn't have `meeting_id` field
        :raise NotFoundError: if meeting doesn't exist
        :raise ConstraintsError: if string length constraints are broken
        :raise ConstraintsError: if foreign key constraints are broken
        """
        # make parameters preparation
        if "busy_status" in update_parameters:
            update_parameters["busy_status"] = self._prepare_busy_status(
                busy_status_name=update_parameters["busy_status"]
            )

        if "participants_emails" in update_parameters:
            update_parameters["participants_ids"] = self._prepare_participants_ids(
                participants_emails=update_parameters["participants_emails"]
            )
            update_parameters.pop("participants_emails")

        with self.connection:
            meeting = self.meeting_dao.get_by_id(meeting_id=meeting_id)
            self._check_access(user_with_access_id=meeting.owner_id)
            meeting_update_input = meeting.update(**update_parameters)
            updated_meeting = self.meeting_dao.update(meeting_update_input)
        return updated_meeting

    def delete(self, meeting_id: int):
        """Delete the meeting.

        :param meeting_id: id to delete

        :raise AccessError: if meeting can't be accessed by service user
        :raise NotFoundError: if meeting doesn't exist
        """
        with self.connection:
            meeting = self.meeting_dao.get_by_id(meeting_id=meeting_id)
            self._check_access(user_with_access_id=meeting.owner_id)
            self.meeting_dao.delete(meeting_id=meeting_id)

    def read_invitation(self, meeting_id: int, user_id: int) -> AcceptStatus:
        """Accept the invitation to the meeting.

        :param meeting_id: meeting id
        :param user_id: participant id

        :return: invitation status

        :raise NotFoundError: if meeting doesn't exist
        :raise NotFoundError: if user doesn't have invitation to this meeting
        """
        with self.connection:
            return self.meeting_dao.read_invitation(
                meeting_id=meeting_id, user_id=user_id
            )

    def answer_invitation(
        self, meeting_id: int, user_id: int, accept_status: str
    ) -> AcceptStatus:
        """Answer to the invitation to the meeting.

        :param meeting_id: meeting id
        :param user_id: participant id
        :param accept_status: name of status of accepting

        :return: accept status that was set

        :raise PreparationError: if preparation of accept_status to the DAO object fails
        :raise NotFoundError: if meeting doesn't exist
        :raise NotFoundError: if user doesn't have invitation to this meeting
        """
        with self.connection:
            self._check_access(user_with_access_id=user_id)
            accept_status = self._prepare_accept_status(
                accept_status_name=accept_status
            )
            self.meeting_dao.answer_invitation(
                meeting_id=meeting_id, user_id=user_id, accept_status=accept_status
            )

        return accept_status
