from typing import Any
from typing import Dict
from typing import Optional

from meet_up.dao.user import UserDAO
from meet_up.domain.user import User
from meet_up.services.exceptions import AccessError


class UserService:
    """Services related to user.

    This class also handles the transactions over DAO objects.
    """

    def __init__(self, user_dao: UserDAO, logged_user_email: Optional[str] = None):
        """Init object.

        :param user_dao: DAO for user
        :param logged_user_email: email of service user
        """
        self.user_dao = user_dao
        self.connection = user_dao.connection
        self.logged_user_email = logged_user_email

    def _check_access(self, user_with_access_id: int):
        """Check if service user has access to the operation.

        :param user_with_access_id: user that can do this action
        """
        logged_user = self.user_dao.get_by_email(email=self.logged_user_email)
        if self.logged_user_email is None or (
            not logged_user.is_admin and user_with_access_id != logged_user.user_id
        ):
            raise AccessError(
                f"Only user with user_id={user_with_access_id} can make this operation"
            )

    def get_by_id(self, user_id: int) -> User:
        """Get user by id.

        :param user_id: id to search

        :return: found user

        :raise NotFoundError: if user doesn't exist
        :raise ValidationError: if user creation fails
        """
        with self.connection:
            user = self.user_dao.get_by_id(user_id=user_id)
        return user

    def get_by_email(self, email: str) -> User:
        """Get user by email.

        :param email: email to search

        :return: found user

        :raise NotFoundError: if user doesn't exist
        :raise ValidationError: if user creation fails
        """
        with self.connection:
            user = self.user_dao.get_by_email(email=email)
        return user

    def create(
        self,
        email: str,
        name: str,
        surname: str,
        patronymic: Optional[str],
        password: str,
    ) -> User:
        """Create user.

        :param email: email of the user
        :param name: name of the user
        :param surname: surname of the user
        :param patronymic: patronymic of the user
        :param password: password of the user

        :return: created user

        :raise ValidationError: if user creation fails
        :raise ConstraintsError: if string length constraints are broken
        :raise ConstraintsError: if unique constraint is broken on email
        """
        user = User(
            email=email,
            name=name,
            surname=surname,
            patronymic=patronymic,
            password=password,
        )

        with self.connection:
            created_user = self.user_dao.create(user)
        return created_user

    def update(self, user_id: int, update_parameters: Dict[str, Any]) -> User:
        """Update user.

        :param user_id: user id to update

        :return: updated user

        :raise AccessError: if user can't be accessed by service user
        :raise ValidationError: if user creation fails
        :raise MissingIdentifier: if given user doesn't have `user_id` field
        :raise NotFoundError: if user doesn't exist
        :raise ConstraintsError: if string length constraints are broken
        :raise ConstraintsError: if unique constraint is broken on email
        """
        with self.connection:
            self._check_access(user_with_access_id=user_id)
            user = self.user_dao.get_by_id(user_id=user_id)
            user_update_input = user.update(**update_parameters)
            updated_user = self.user_dao.update(user_update_input)
        return updated_user

    def delete(self, user_id: int):
        """Delete the user.

        :param user_id: id to delete

        :raise AccessError: if user can't be accessed by service user
        :raise NotFoundError: if user doesn't exist
        """
        with self.connection:
            self._check_access(user_with_access_id=user_id)
            self.user_dao.delete(user_id=user_id)
