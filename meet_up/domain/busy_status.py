from enum import Enum


class BusyStatus(int, Enum):
    """Status of busyness of the meeting."""

    FREE = 0
    BUSY = 1
    NOT_HERE = 2
