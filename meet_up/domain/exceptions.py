class ValidationError(Exception):
    """Problem with validation of the object."""

    def __init__(self, message: str):
        super().__init__(message)
