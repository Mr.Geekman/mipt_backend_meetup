from enum import Enum


class AcceptStatus(int, Enum):
    """Status of accept of the meeting."""

    THINKING = 0
    ACCEPTED = 1
    DECLINED = 2
