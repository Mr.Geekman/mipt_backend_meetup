import hashlib
from dataclasses import InitVar
from dataclasses import dataclass
from dataclasses import field
from dataclasses import replace
from hmac import compare_digest
from typing import Any
from typing import Dict
from typing import Optional

from meet_up.domain.exceptions import ValidationError


@dataclass()
class User:
    """Class for modeling the user."""

    email: str
    name: str
    surname: str

    user_id: Optional[int] = field(default=None)
    is_admin: bool = field(default=False)
    password_hash: str = field(repr=False, default=None)
    password: InitVar[str] = field(default=None)
    patronymic: Optional[str] = field(default=None)

    update_fields = {
        "email",
        "name",
        "surname",
        "patronymic",
        "password_hash",
        "password",
    }

    def update(self, **update_parameters: Dict[str, Any]) -> "User":
        """Create a new user by updating this.

        Only parameters listed in `update_fields` will be updated.

        :param update_parameters: parameters to update

        :return: new user with updated fields
        """
        update_parameters = {
            key: value
            for key, value in update_parameters.items()
            if key in self.update_fields
        }

        return replace(self, **update_parameters)

    @staticmethod
    def make_password_hash(password: str) -> str:
        """Generate password hash by password.

        :param password: password

        :return: generated hash
        """
        return hashlib.sha256(password.encode("utf-8")).hexdigest()

    def __post_init__(self, password: str):
        """Make post-init class processing.

        Here we also have basic validation.

        :param password: password to make hash

        :raise ValidationError: if validation fails
        """
        if password is not None:
            self.password_hash = self.make_password_hash(password)
        else:
            if self.password_hash is None:
                raise ValidationError(
                    "The value of password or password_hash should be set"
                )

    def check_password(self, password) -> bool:
        """Check given password fits this user.

        :param password: password

        :return: is password fits
        """
        hashed_password = self.make_password_hash(password)
        return compare_digest(hashed_password, self.password_hash)
