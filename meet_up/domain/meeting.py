import datetime
from dataclasses import dataclass
from dataclasses import field
from dataclasses import replace
from typing import Any
from typing import Dict
from typing import Optional
from typing import Set

from meet_up.domain.busy_status import BusyStatus
from meet_up.domain.exceptions import ValidationError


@dataclass()
class Meeting:
    """Class for modeling the meeting."""

    place: str
    description: str
    start_datetime: datetime.datetime
    end_datetime: datetime.datetime
    owner_id: int

    busy_status: BusyStatus = field(default=BusyStatus.BUSY)
    meeting_id: Optional[int] = field(default=None)
    period_week: Optional[int] = field(default=None)
    # TODO: think about addding statuses here
    participants_ids: Set[int] = field(default_factory=set)

    update_fields = {
        "place",
        "description",
        "start_datetime",
        "end_datetime",
        "period_week",
        "participants_ids",
        "busy_status",
    }

    def update(self, **update_parameters: Dict[str, Any]) -> "Meeting":
        """Create a new meeting by updating this.

        Only parameters listed in `update_fields` will be updated.

        :param update_parameters: parameters to update

        :return: new meeting with updated fields
        """
        update_parameters = {
            key: value
            for key, value in update_parameters.items()
            if key in self.update_fields
        }

        return replace(self, **update_parameters)

    def __post_init__(self):
        """Make post-init class processing.

        Here we also have basic validation.

        :raise ValidationError: if validation fails
        """
        self.participants_ids.add(self.owner_id)

        # check start_datetime < end_datetime
        if self.start_datetime >= self.end_datetime:
            raise ValidationError(
                "The value of start_datetime should be less than for end_datetime"
            )

        # check that period_week is positive
        if self.period_week is not None and self.period_week <= 0:
            raise ValidationError("Period of week should be positive")
