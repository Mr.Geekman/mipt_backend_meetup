from psycopg2.errors import StringDataRightTruncation
from psycopg2.errors import UniqueViolation

from meet_up.dao.exceptions import ConstraintsError
from meet_up.dao.exceptions import MissingIdentifier
from meet_up.dao.exceptions import NotFoundError
from meet_up.domain.user import User


class UserDAO:
    """DAO object for user."""

    def __init__(self, connection):
        """Init object.

        :param connection: connection to use
        """
        self.connection = connection

    def get_by_id(self, user_id: int) -> User:
        """Get user by id.

        :param user_id: id to search

        :return: found user

        :raise NotFoundError: if user doesn't exist
        :raise ValidationError: if user creation fails
        """
        with self.connection.cursor() as cursor:
            cursor.execute("SELECT * FROM users WHERE user_id = %s;", (user_id,))
            row = cursor.fetchone()
            if row is None:
                raise NotFoundError(f"User with id={user_id} doesn't exist")

            return User(
                user_id=row[0],
                email=row[1],
                password_hash=row[2],
                is_admin=row[3],
                name=row[4],
                surname=row[5],
                patronymic=row[6],
            )

    def get_by_email(self, email: str) -> User:
        """Get user by email.

        :param email: email to search

        :return: found user

        :raise NotFoundError: if user doesn't exist
        :raise ValidationError: if user creation fails
        """
        with self.connection.cursor() as cursor:
            cursor.execute("SELECT * FROM users WHERE email = %s;", (email,))
            row = cursor.fetchone()
            if row is None:
                raise NotFoundError(f"User with email={email} doesn't exist")

            return User(
                user_id=row[0],
                email=row[1],
                password_hash=row[2],
                is_admin=row[3],
                name=row[4],
                surname=row[5],
                patronymic=row[6],
            )

    def create(
        self,
        user: User,
    ) -> User:
        """Create user.

        :param user: user domain representation

        :return: created user

        :raise ConstraintsError: if string length constraints are broken
        :raise ConstraintsError: if unique constraint is broken on email
        """
        with self.connection.cursor() as cursor:
            try:
                cursor.execute(
                    """
                    INSERT INTO users(user_id, email, password_hash, is_admin, name, surname, patronymic) VALUES 
                    (DEFAULT, %s, %s, %s, %s, %s, %s)
                    RETURNING user_id;
                """,
                    (
                        user.email,
                        user.password_hash,
                        user.is_admin,
                        user.name,
                        user.surname,
                        user.patronymic,
                    ),
                )
                new_id = cursor.fetchone()[0]

            except (UniqueViolation, StringDataRightTruncation) as e:
                raise ConstraintsError(str(e))

        created_user = self.get_by_id(new_id)
        return created_user

    def update(self, user: User) -> User:
        """Update user.

        :param user: user domain representation

        :return: updated user

        :raise MissingIdentifier: if given user doesn't have `user_id` field
        :raise NotFoundError: if user doesn't exist
        :raise ConstraintsError: if string length constraints are broken
        :raise ConstraintsError: if unique constraint is broken on email
        """
        # check if user exists
        if user.user_id is None:
            raise MissingIdentifier("Given user should have `user_id` field")

        # check if user exists
        _ = self.get_by_id(user_id=user.user_id)

        # update user
        with self.connection.cursor() as cursor:
            try:
                cursor.execute(
                    """
                    UPDATE users 
                    SET 
                        email = %s,
                        password_hash = %s,
                        name = %s,
                        surname = %s,
                        patronymic = %s
                    WHERE user_id = %s;
                """,
                    (
                        user.email,
                        user.password_hash,
                        user.name,
                        user.surname,
                        user.patronymic,
                        user.user_id,
                    ),
                )

            except (UniqueViolation, StringDataRightTruncation) as e:
                raise ConstraintsError(str(e))

        updated_user = self.get_by_id(user_id=user.user_id)
        return updated_user

    def delete(self, user_id: int):
        """Delete the user.

        :param user_id: id to delete

        :raise NotFoundError: if user doesn't exist
        """
        # check user on existence
        _ = self.get_by_id(user_id=user_id)

        # make removal
        with self.connection.cursor() as cursor:
            cursor.execute(
                "DELETE FROM users WHERE user_id = %s",
                (user_id,),
            )
