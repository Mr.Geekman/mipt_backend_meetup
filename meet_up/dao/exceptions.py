class NotFoundError(Exception):
    """Object isn't found in database."""

    def __init__(self, message: str):
        super().__init__(message)


class ConstraintsError(Exception):
    """There is a broken constraint within database."""

    def __init__(self, message: str):
        super().__init__(message)


class MissingIdentifier(Exception):
    """There is a missing identifier for update."""

    def __init__(self, message: str):
        super().__init__(message)
