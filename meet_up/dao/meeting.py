from psycopg2.errors import ForeignKeyViolation
from psycopg2.errors import StringDataRightTruncation

from meet_up.dao.exceptions import ConstraintsError
from meet_up.dao.exceptions import MissingIdentifier
from meet_up.dao.exceptions import NotFoundError
from meet_up.domain.accept_status import AcceptStatus
from meet_up.domain.busy_status import BusyStatus
from meet_up.domain.meeting import Meeting


class MeetingDAO:
    """DAO object for meeting."""

    def __init__(self, connection):
        """Init object.

        :param connection: connection to use
        """
        self.connection = connection

    def get_by_id(self, meeting_id: int) -> Meeting:
        """Get meeting by id.

        :param meeting_id: id to search

        :return: found meeting

        :raise NotFoundError: if meeting doesn't exist
        :raise ValidationError: if meeting creation fails
        """
        with self.connection.cursor() as cursor:
            # get meeting
            cursor.execute(
                "SELECT * FROM meetings WHERE meeting_id = %s;", (meeting_id,)
            )
            row = cursor.fetchone()
            if row is None:
                raise NotFoundError(f"Meeting with id={meeting_id} doesn't exist")

            meeting = Meeting(
                meeting_id=row[0],
                place=row[1],
                description=row[2],
                start_datetime=row[3],
                end_datetime=row[4],
                period_week=row[5],
                owner_id=row[6],
                busy_status=BusyStatus(row[7]),
            )

            # get meeting participants
            participants_ids = set()
            cursor.execute(
                "Select * FROM meetings_users WHERE meeting_id = %s", (meeting_id,)
            )
            rows = cursor.fetchall()
            for row in rows:
                participants_ids.add(row[0])
            meeting.participants_ids = participants_ids
            return meeting

    def create(
        self,
        meeting: Meeting,
    ) -> Meeting:
        """Create meeting.

        :param meeting: meeting domain representation

        :return: created meeting

        :raise ConstraintsError: if string length constraints are broken
        :raise ConstraintsError: if foreign key constraints are broken
        """
        with self.connection.cursor() as cursor:
            try:
                cursor.execute(
                    """
                    INSERT INTO meetings(meeting_id, place, description, start_datetime, end_datetime, period_week, owner_id, busy_status_id) VALUES 
                    (DEFAULT, %s, %s, %s, %s, %s, %s, %s)
                    RETURNING meeting_id;
                """,
                    (
                        meeting.place,
                        meeting.description,
                        meeting.start_datetime,
                        meeting.end_datetime,
                        meeting.period_week,
                        meeting.owner_id,
                        meeting.busy_status.value,
                    ),
                )
                meeting_id = cursor.fetchone()[0]

                # add accepted status for the owner of the meeting
                cursor.execute(
                    "INSERT INTO meetings_users(user_id, meeting_id, accept_status_id) VALUES (%s, %s, %s)",
                    (meeting.owner_id, meeting_id, AcceptStatus.ACCEPTED.value),
                )

                # add thinking status for other participants
                for participant_id in meeting.participants_ids.difference(
                    {meeting.owner_id}
                ):
                    cursor.execute(
                        "INSERT INTO meetings_users(user_id, meeting_id, accept_status_id) VALUES (%s, %s, %s)",
                        (participant_id, meeting_id, AcceptStatus.THINKING.value),
                    )

            except (ForeignKeyViolation, StringDataRightTruncation) as e:
                raise ConstraintsError(str(e))

        return self.get_by_id(meeting_id=meeting_id)

    def update(self, meeting: Meeting) -> Meeting:
        """Update meeting.

        :param meeting: meeting domain representation

        :return: updated meeting

        :raise MissingIdentifier: if given meeting doesn't have `meeting_id` field
        :raise NotFoundError: if meeting doesn't exist
        :raise ConstraintsError: if string length constraints are broken
        :raise ConstraintsError: if foreign key constraints are broken
        """
        # check if user exists
        if meeting.meeting_id is None:
            raise MissingIdentifier("Given meeting should have `meeting_id` field")

        # check if user exists
        _ = self.get_by_id(meeting_id=meeting.meeting_id)

        with self.connection.cursor() as cursor:
            try:
                cursor.execute(
                    """
                    UPDATE meetings 
                    SET 
                        place = %s,
                        description = %s,
                        start_datetime = %s,
                        end_datetime = %s,
                        period_week = %s,
                        owner_id = %s,
                        busy_status_id = %s
                    WHERE meeting_id = %s;
                """,
                    (
                        meeting.place,
                        meeting.description,
                        meeting.start_datetime,
                        meeting.end_datetime,
                        meeting.period_week,
                        meeting.owner_id,
                        meeting.busy_status.value,
                        meeting.meeting_id,
                    ),
                )

                # remove all old statuses
                cursor.execute(
                    "DELETE FROM meetings_users WHERE meeting_id = %s",
                    (meeting.meeting_id,),
                )

                # add accepted status for the owner of the meeting
                cursor.execute(
                    "INSERT INTO meetings_users(user_id, meeting_id, accept_status_id) VALUES (%s, %s, %s)",
                    (
                        meeting.owner_id,
                        meeting.meeting_id,
                        AcceptStatus.ACCEPTED.value,
                    ),
                )

                # add thinking status for other participants
                for participant_id in meeting.participants_ids.difference(
                    {meeting.owner_id}
                ):
                    cursor.execute(
                        "INSERT INTO meetings_users(user_id, meeting_id, accept_status_id) VALUES (%s, %s, %s)",
                        (
                            participant_id,
                            meeting.meeting_id,
                            AcceptStatus.THINKING.value,
                        ),
                    )

            except (ForeignKeyViolation, StringDataRightTruncation) as e:
                raise ConstraintsError(str(e))

        new_meeting = self.get_by_id(meeting_id=meeting.meeting_id)
        return new_meeting

    def delete(self, meeting_id: int):
        """Delete the meeting.

        :param meeting_id: id to delete

        :raise NotFoundError: if meeting doesn't exist
        """
        # check meeting on existence
        _ = self.get_by_id(meeting_id=meeting_id)

        # make removal
        with self.connection.cursor() as cursor:
            cursor.execute(
                "DELETE FROM meetings WHERE meeting_id = %s",
                (meeting_id,),
            )

    def read_invitation(self, meeting_id: int, user_id: int) -> AcceptStatus:
        """Read the invitation to the meeting.

        :param meeting_id: meeting id
        :param user_id: participant id

        :return: invitation status

        :raise NotFoundError: if meeting doesn't exist
        :raise NotFoundError: if user doesn't have invitation to this meeting
        """
        meeting = self.get_by_id(meeting_id=meeting_id)
        if user_id not in meeting.participants_ids:
            raise NotFoundError("This user isn't participating in this meeting")

        with self.connection.cursor() as cursor:
            cursor.execute(
                "SELECT accept_status_id FROM meetings_users WHERE meeting_id = %s AND user_id = %s;",
                (
                    meeting.meeting_id,
                    user_id,
                ),
            )
            accept_status_id = cursor.fetchone()[0]

        return AcceptStatus(accept_status_id)

    def answer_invitation(
        self, meeting_id: int, user_id: int, accept_status: AcceptStatus
    ):
        """Answer to the invitation to the meeting.

        :param meeting_id: meeting id
        :param user_id: participant id
        :param accept_status: status of accepting

        :raise NotFoundError: if meeting doesn't exist
        :raise NotFoundError: if user doesn't have invitation to this meeting
        """
        meeting = self.get_by_id(meeting_id=meeting_id)
        if user_id not in meeting.participants_ids:
            raise NotFoundError("This user isn't participating in this meeting")

        with self.connection.cursor() as cursor:
            cursor.execute(
                """
            UPDATE meetings_users
            SET 
                accept_status_id = %s
            WHERE meeting_id = %s AND user_id = %s;
            """,
                (
                    accept_status.value,
                    meeting.meeting_id,
                    user_id,
                ),
            )
