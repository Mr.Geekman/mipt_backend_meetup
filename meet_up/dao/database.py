import psycopg2

from meet_up.config import CONFIG
from meet_up.domain.accept_status import AcceptStatus
from meet_up.domain.busy_status import BusyStatus
from meet_up.domain.user import User


def get_connection():
    """Get connection with postgres database.

    :return: connection
    """
    db_config = CONFIG["database"]
    conn = psycopg2.connect(**db_config)
    return conn


def create_tables(connection):
    """Create empty tables according to the app architecture.

    :param connection: connection to work with
    """
    with connection as connection:
        with connection.cursor() as cursor:

            # drop all tables
            cursor.execute(
                """
                DROP TABLE IF EXISTS meetings_users;
                DROP TABLE IF EXISTS accept_statuses;
                DROP TABLE IF EXISTS meetings;
                DROP TABLE IF EXISTS busy_statuses;
                DROP TABLE IF EXISTS users;
            """
            )

            # add users table
            cursor.execute(
                """
                CREATE TABLE users(
                    user_id SERIAL PRIMARY KEY,
                    email VARCHAR(255) UNIQUE NOT NULL,
                    password_hash VARCHAR(100) NOT NULL,
                    is_admin boolean NOT NULL,
                    name VARCHAR(50) NOT NULL,
                    surname VARCHAR(50) NOT NULL,
                    patronymic VARCHAR(50)
                );
            """
            )

            # add super_user
            email = CONFIG["super_user"]["email"]
            password = CONFIG["super_user"]["password"]
            password_hash = User.make_password_hash(password)
            cursor.execute(
                """
                INSERT INTO users(user_id, email, password_hash, is_admin, name, surname, patronymic) VALUES 
                (DEFAULT, %s, %s, %s, %s, %s, %s);
            """,
                (email, password_hash, True, "Dmitry", "Bunin", "Alexeevich"),
            )

            # add busy statuses table and its values
            cursor.execute(
                """
                CREATE TABLE busy_statuses(
                    status_id INTEGER PRIMARY KEY,
                    name VARCHAR(10) NOT NULL
                );
            """
            )

            cursor.execute(
                "INSERT INTO busy_statuses(status_id, name) VALUES (%s, %s)",
                (BusyStatus.FREE.value, BusyStatus.FREE.name),
            )
            cursor.execute(
                "INSERT INTO busy_statuses(status_id, name) VALUES (%s, %s)",
                (BusyStatus.BUSY.value, BusyStatus.BUSY.name),
            )
            cursor.execute(
                "INSERT INTO busy_statuses(status_id, name) VALUES (%s, %s)",
                (BusyStatus.NOT_HERE.value, BusyStatus.NOT_HERE.name),
            )

            # add meetings table
            cursor.execute(
                """
                CREATE TABLE meetings(
                    meeting_id SERIAL PRIMARY KEY,
                    place VARCHAR(100),
                    description VARCHAR(1000),
                    start_datetime TIMESTAMP NOT NULL,
                    end_datetime TIMESTAMP NOT NULL,
                    period_week INTEGER,
                    owner_id INTEGER NOT NULL,
                    busy_status_id INTEGER NOT NULL,
                    CONSTRAINT fk_owner
                        FOREIGN KEY(owner_id) 
                        REFERENCES users(user_id)
                        ON DELETE NO ACTION,
                    CONSTRAINT fk_busy_status
                        FOREIGN KEY(busy_status_id) 
                        REFERENCES busy_statuses(status_id)
                        ON DELETE NO ACTION
                );
            """
            )

            # add accept statuses table and its values
            cursor.execute(
                """
                CREATE TABLE accept_statuses(
                    status_id INTEGER PRIMARY KEY,
                    name VARCHAR(10) NOT NULL
                );
            """
            )

            cursor.execute(
                "INSERT INTO accept_statuses(status_id, name) VALUES (%s, %s)",
                (AcceptStatus.THINKING.value, AcceptStatus.THINKING.name),
            )
            cursor.execute(
                "INSERT INTO accept_statuses(status_id, name) VALUES (%s, %s)",
                (AcceptStatus.ACCEPTED.value, AcceptStatus.ACCEPTED.name),
            )
            cursor.execute(
                "INSERT INTO accept_statuses(status_id, name) VALUES (%s, %s)",
                (AcceptStatus.DECLINED.value, AcceptStatus.DECLINED.name),
            )

            # add meetings_users table
            cursor.execute(
                """
                CREATE TABLE meetings_users(
                    user_id INTEGER,
                    meeting_id INTEGER,
                    accept_status_id INTEGER,
                    CONSTRAINT fk_user
                        FOREIGN KEY(user_id) 
                        REFERENCES users(user_id)
                        ON DELETE CASCADE,
                    CONSTRAINT fk_meeting
                        FOREIGN KEY(meeting_id) 
                        REFERENCES meetings(meeting_id)
                        ON DELETE CASCADE,
                    CONSTRAINT fk_accept_status
                        FOREIGN KEY(accept_status_id) 
                        REFERENCES accept_statuses(status_id)
                        ON DELETE NO ACTION
                );
            """
            )
