import pathlib
from typing import Any
from typing import Dict

import yaml

CONFIG_PATH = (
    pathlib.Path(__file__).parent.parent.joinpath("configs").joinpath("config.yaml")
)


def load_config() -> Dict[str, Any]:
    with open(CONFIG_PATH, "r") as inf:
        config = yaml.safe_load(inf)
    return config


CONFIG = load_config()
