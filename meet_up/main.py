import click
from psycopg2 import OperationalError

from meet_up.api.app import app
from meet_up.dao.database import create_tables
from meet_up.dao.database import get_connection


@click.group()
def cli():
    pass


@cli.command()
def create_database():
    """Create and prepare database."""
    try:
        connection = get_connection()
        create_tables(connection)
    except OperationalError:
        print("Can't get connection to the database, try start the database first")


@cli.command()
def start_server():
    """Start the server."""
    app.run()


if __name__ == "__main__":
    start_server()
