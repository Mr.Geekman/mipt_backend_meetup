from flask import Flask
from flask_jwt_extended import JWTManager
from flask_restx import Api
from werkzeug.middleware.proxy_fix import ProxyFix

from meet_up.api.resources import ns
from meet_up.config import CONFIG

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(
    app,
    version="0.1",
    title="MeetUp API",
    description="A simple API for creating a meetings",
)

api.add_namespace(ns)
app.config.from_mapping(CONFIG["flask"])
jwt = JWTManager(app)
