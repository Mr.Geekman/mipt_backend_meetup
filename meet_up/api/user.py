from flask_restx import Model
from flask_restx import fields
from flask_restx import inputs
from flask_restx import reqparse

user_model = Model(
    "User",
    {
        "user_id": fields.Integer(
            readonly=True, description="User's unique identifier"
        ),
        "email": fields.String(description="User's email"),
        "name": fields.String(description="Name"),
        "surname": fields.String(description="Surname"),
        "patronymic": fields.String(required=False, description="Patronymic"),
    },
)

jwt_model = Model(
    "JWT",
    {"access_token": fields.String(readonly=True, description="JWT access token")},
)


user_create_parser = reqparse.RequestParser()
user_create_parser.add_argument(
    "email", type=inputs.email(), required=True, location="json"
)
user_create_parser.add_argument("name", type=str, required=True, location="json")
user_create_parser.add_argument("surname", type=str, required=True, location="json")
user_create_parser.add_argument("patronymic", type=str, required=False, location="json")
user_create_parser.add_argument("password", type=str, required=True, location="json")

user_patch_parser = reqparse.RequestParser()
user_patch_parser.add_argument(
    "email", type=inputs.email(), required=False, location="json"
)
user_patch_parser.add_argument("name", type=str, required=False, location="json")
user_patch_parser.add_argument("surname", type=str, required=False, location="json")
user_patch_parser.add_argument("patronymic", type=str, required=False, location="json")
user_patch_parser.add_argument("password", type=str, required=False, location="json")

user_login_parser = reqparse.RequestParser()
user_login_parser.add_argument(
    "email", type=inputs.email(), required=True, location="json"
)
user_login_parser.add_argument("password", type=str, required=True, location="json")
