class CredentialsError(Exception):
    """Wrong login credentials."""

    def __init__(self, message: str):
        super().__init__(message)
