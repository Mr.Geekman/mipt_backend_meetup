from flask_restx import Model
from flask_restx import fields
from flask_restx import inputs
from flask_restx import reqparse

from meet_up.domain.busy_status import BusyStatus

meeting_model = Model(
    "Meeting",
    {
        "meeting_id": fields.Integer(
            readonly=True, description="Meeting's unique identifier"
        ),
        "place": fields.String(description="Place of the meeting"),
        "description": fields.String(description="Description of the meeting"),
        "start_datetime": fields.DateTime(description="Start of the meeting"),
        "end_datetime": fields.DateTime(description="End of the meeting"),
        "owner_id": fields.Integer(
            readonly=True, description="User identifier of the creator of the meeting"
        ),
        "period_week": fields.Integer(
            required=False,
            description="Determines the number of weeks between recurring meetings",
        ),
        "participants_ids": fields.List(
            fields.Integer(description="User identifier of the participant"),
            description="List of identifiers of the participants of the meeting",
        ),
        "busy_status": fields.String(
            description=f"How to determine time of meeting, possible values: {[e.name for e in BusyStatus]}",
            attribute="busy_status.name",
        ),
    },
)

meeting_create_parser = reqparse.RequestParser()
meeting_create_parser.add_argument("place", type=str, required=True, location="json")
meeting_create_parser.add_argument(
    "description", type=str, required=True, location="json"
)
meeting_create_parser.add_argument(
    "start_datetime", type=inputs.datetime_from_iso8601, required=True, location="json"
)
meeting_create_parser.add_argument(
    "end_datetime", type=inputs.datetime_from_iso8601, required=True, location="json"
)
meeting_create_parser.add_argument("owner_id", type=int, required=True, location="json")
meeting_create_parser.add_argument(
    "period_week", type=inputs.positive, required=False, location="json"
)
# TODO: I haven't figured out how to turn this into List[inputs.email()] as wanted
meeting_create_parser.add_argument(
    "participants_emails",
    type=list,
    required=True,
    location="json",
)
meeting_create_parser.add_argument(
    "busy_status", type=str, required=True, location="json"
)

meeting_patch_parser = reqparse.RequestParser()
meeting_patch_parser.add_argument("place", type=str, required=False, location="json")
meeting_patch_parser.add_argument(
    "description", type=str, required=False, location="json"
)
meeting_patch_parser.add_argument(
    "start_datetime", type=inputs.datetime_from_iso8601, required=False, location="json"
)
meeting_patch_parser.add_argument(
    "end_datetime", type=inputs.datetime_from_iso8601, required=False, location="json"
)
meeting_patch_parser.add_argument("owner_id", type=int, required=False, location="json")
meeting_patch_parser.add_argument(
    "period_week", type=inputs.positive, required=False, location="json"
)
# TODO: I haven't figured out how to turn this into List[inputs.email()] as wanted
meeting_patch_parser.add_argument(
    "participants_emails",
    type=list,
    required=False,
    location="json",
)
meeting_patch_parser.add_argument(
    "busy_status", type=str, required=False, location="json"
)
