from flask_restx import Model
from flask_restx import fields
from flask_restx import reqparse

from meet_up.domain.accept_status import AcceptStatus

invitation_model = Model(
    "Invitation",
    {
        "status": fields.String(
            description=f"Current status of the invitation to the meeting, possible values: {[e.name for e in AcceptStatus]}",
            attribute="name",
        ),
    },
)


invitation_parser = reqparse.RequestParser()
invitation_parser.add_argument("status", type=str, required=True, location="json")
