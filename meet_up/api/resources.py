from http import HTTPStatus

from flask import request
from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required
from flask_jwt_extended.exceptions import NoAuthorizationError
from flask_restx import Namespace
from flask_restx import Resource

from meet_up.api.exceptions import CredentialsError
from meet_up.api.invitation import invitation_model
from meet_up.api.invitation import invitation_parser
from meet_up.api.meeting import meeting_create_parser
from meet_up.api.meeting import meeting_model
from meet_up.api.meeting import meeting_patch_parser
from meet_up.api.user import jwt_model
from meet_up.api.user import user_create_parser
from meet_up.api.user import user_login_parser
from meet_up.api.user import user_model
from meet_up.api.user import user_patch_parser
from meet_up.dao.database import get_connection
from meet_up.dao.exceptions import ConstraintsError
from meet_up.dao.exceptions import NotFoundError
from meet_up.dao.meeting import MeetingDAO
from meet_up.dao.user import UserDAO
from meet_up.services.exceptions import PreparationError
from meet_up.services.meeting import MeetingService
from meet_up.services.user import UserService

ns = Namespace("api", description="Namespace of the main API")

# add models
ns.add_model("User", user_model)
ns.add_model("Meeting", meeting_model)
ns.add_model("JWT", jwt_model)
ns.add_model("Invitation", invitation_model)


@ns.route("/login")
@ns.response(HTTPStatus.CREATED.value, "Access token is created")
@ns.response(HTTPStatus.UNAUTHORIZED.value, "Wrong login credentials")
class UserLogin(Resource):
    """Resource for logging in."""

    @ns.doc("Login a user by getting access token")
    @ns.marshal_with(jwt_model)
    @ns.expect(user_login_parser)
    def post(self):
        args = user_login_parser.parse_args()
        email = args.email
        password = args.password

        user_dao = UserDAO(get_connection())
        user_service = UserService(user_dao)

        try:
            user = user_service.get_by_email(email=email)
            if user.check_password(password=password):
                access_token = create_access_token(identity=email)
                return {"access_token": access_token}, HTTPStatus.CREATED
            else:
                raise CredentialsError("Wrong password")

        except NotFoundError:
            raise CredentialsError("Unknown user")


@ns.route("/users/<int:user_id>")
@ns.response(HTTPStatus.NOT_FOUND.value, "User not found")
@ns.response(HTTPStatus.BAD_REQUEST.value, "Wrong request parameters")
@ns.param("user_id", "The user identifier")
class UserResource(Resource):
    """Resource for working with a single user."""

    @ns.doc("Get user data")
    @ns.marshal_with(user_model)
    def get(self, user_id: int):
        user_dao = UserDAO(get_connection())
        user_service = UserService(user_dao)
        user = user_service.get_by_id(user_id=user_id)
        return user

    @jwt_required()
    @ns.doc("Delete the user")
    @ns.response(HTTPStatus.NO_CONTENT.value, "Nothing to return")
    def delete(self, user_id: int):
        logged_user_email = get_jwt_identity()

        user_dao = UserDAO(get_connection())
        user_service = UserService(user_dao, logged_user_email=logged_user_email)
        user_service.delete(user_id=user_id)
        return {}, HTTPStatus.NO_CONTENT

    @jwt_required()
    @ns.doc("Update the user data")
    @ns.marshal_with(user_model)
    @ns.expect(user_patch_parser)
    def patch(self, user_id: int):
        logged_user_email = get_jwt_identity()
        args = user_patch_parser.parse_args()

        # select only parameters that have been passed
        raw_args = request.get_json()
        dict_args = {key: value for key, value in dict(args).items() if key in raw_args}

        user_dao = UserDAO(get_connection())
        user_service = UserService(user_dao, logged_user_email=logged_user_email)
        user = user_service.update(user_id=user_id, update_parameters=dict_args)

        return user


@ns.route("/users")
@ns.response(HTTPStatus.CREATED.value, "User is created")
@ns.response(HTTPStatus.BAD_REQUEST.value, "Wrong request parameters")
class UsersResource(Resource):
    """Resource for working with users in general."""

    @ns.doc("Create a user")
    @ns.marshal_with(user_model)
    @ns.expect(user_create_parser)
    def post(self):
        args = user_create_parser.parse_args()

        user_dao = UserDAO(get_connection())
        user_service = UserService(user_dao)
        user = user_service.create(**dict(args))

        return user, HTTPStatus.CREATED


@ns.route("/meetings/<int:meeting_id>")
@ns.response(HTTPStatus.NOT_FOUND.value, "Meeting not found")
@ns.param("meeting_id", "The user identifier")
class MeetingResource(Resource):
    """Resource for working with a single meeting."""

    @ns.doc("Get meeting data")
    @ns.marshal_with(meeting_model)
    def get(self, meeting_id: int):
        connection = get_connection()
        user_dao = UserDAO(connection)
        meeting_dao = MeetingDAO(connection)
        meeting_service = MeetingService(meeting_dao=meeting_dao, user_dao=user_dao)
        meeting = meeting_service.get_by_id(meeting_id=meeting_id)
        return meeting

    @jwt_required()
    @ns.doc("Delete the meeting")
    def delete(self, meeting_id: int):
        logged_user_email = get_jwt_identity()

        connection = get_connection()
        user_dao = UserDAO(get_connection())
        meeting_dao = MeetingDAO(connection)
        meeting_service = MeetingService(
            meeting_dao=meeting_dao,
            user_dao=user_dao,
            logged_user_email=logged_user_email,
        )
        meeting_service.delete(meeting_id=meeting_id)
        return {}, HTTPStatus.NO_CONTENT

    @jwt_required()
    @ns.doc("Update the meeting data")
    @ns.response(HTTPStatus.NO_CONTENT.value, "Nothing to return")
    @ns.marshal_with(meeting_model)
    @ns.expect(meeting_patch_parser)
    def patch(self, meeting_id: int):
        logged_user_email = get_jwt_identity()
        args = meeting_patch_parser.parse_args()

        # select only parameters that have been passed
        raw_args = request.get_json()
        dict_args = {key: value for key, value in dict(args).items() if key in raw_args}

        connection = get_connection()
        user_dao = UserDAO(connection)
        meeting_dao = MeetingDAO(connection)
        meeting_service = MeetingService(
            meeting_dao=meeting_dao,
            user_dao=user_dao,
            logged_user_email=logged_user_email,
        )
        meeting = meeting_service.update(
            meeting_id=meeting_id, update_parameters=dict_args
        )

        return meeting


@ns.route("/meetings")
@ns.response(HTTPStatus.CREATED.value, "Meeting is created")
@ns.response(HTTPStatus.BAD_REQUEST.value, "Wrong request parameters")
class MeetingsResource(Resource):
    """Resource for working with meetings in general."""

    @ns.doc("Create a meeting")
    @ns.marshal_with(meeting_model)
    @ns.expect(meeting_create_parser)
    def post(self):
        args = meeting_create_parser.parse_args()

        connection = get_connection()
        user_dao = UserDAO(connection)
        meeting_dao = MeetingDAO(connection)
        meeting_service = MeetingService(meeting_dao=meeting_dao, user_dao=user_dao)
        meeting = meeting_service.create(**dict(args))

        return meeting, HTTPStatus.CREATED


@ns.route("/meetings/<int:meeting_id>/users/<int:user_id>/invitation")
@ns.response(
    HTTPStatus.NOT_FOUND.value, "Meeting not found or user doesn't have this invitation"
)
class InvitationResource(Resource):
    """Resource for working with participant accept status."""

    @ns.doc("Read invitation")
    @ns.marshal_with(invitation_model)
    def get(self, meeting_id: int, user_id: int):
        connection = get_connection()
        user_dao = UserDAO(connection)
        meeting_dao = MeetingDAO(connection)
        meeting_service = MeetingService(
            meeting_dao=meeting_dao,
            user_dao=user_dao,
        )
        accept_status = meeting_service.read_invitation(
            meeting_id=meeting_id, user_id=user_id
        )
        return accept_status

    @jwt_required()
    @ns.doc("Answer to invitation")
    @ns.response(HTTPStatus.BAD_REQUEST.value, "Wrong request parameters")
    @ns.marshal_with(invitation_model)
    @ns.expect(invitation_parser)
    def put(self, meeting_id: int, user_id: int):
        logged_user_email = get_jwt_identity()
        args = invitation_parser.parse_args()

        connection = get_connection()
        user_dao = UserDAO(connection)
        meeting_dao = MeetingDAO(connection)
        meeting_service = MeetingService(
            meeting_dao=meeting_dao,
            user_dao=user_dao,
            logged_user_email=logged_user_email,
        )
        accept_status = meeting_service.answer_invitation(
            meeting_id=meeting_id, user_id=user_id, accept_status=args.status
        )
        return accept_status, HTTPStatus.CREATED


# TODO: add resource like /users/user_id/meetings?start_datetime=xxx&end_datetime=xxx
#   show list of meetings between this two taking into account periodicity
#   only get method is needed, method ror MeetingDAO, that returns all meetings per user and then in service postprocess it

# add resource
ns.add_resource(UserResource)
ns.add_resource(UsersResource)
ns.add_resource(MeetingResource)
ns.add_resource(MeetingsResource)
ns.add_resource(InvitationResource)


# add error handling
@ns.errorhandler(NotFoundError)
def handle_not_found_error(error):
    return {"message": str(error)}, HTTPStatus.NOT_FOUND


@ns.errorhandler(ConstraintsError)
def handle_constraints_error(error):
    return {"message": str(error)}, HTTPStatus.BAD_REQUEST


@ns.errorhandler(PreparationError)
def handle_preparation_error(error):
    return {"message": str(error)}, HTTPStatus.BAD_REQUEST


@ns.errorhandler(CredentialsError)
def handle_credentials_error(error):
    return {"message": str(error)}, HTTPStatus.UNAUTHORIZED


@ns.errorhandler(NoAuthorizationError)
def handle_no_authorized_error(error):
    return {"message": str(error)}, HTTPStatus.UNAUTHORIZED
